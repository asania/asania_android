package com.asania.app.helperclasses.ui.helper;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asania.R;
import com.asania.app.activities.BaseActivity;
import com.asania.app.activities.HomeActivity;
import com.asania.app.widget.AnyTextView;



/**
 * Created by khanhamza on 02-Mar-17.
 */

public class TitleBar extends RelativeLayout {


    TextView btnLeft1;
    TextView btnLeft2;
    AnyTextView txtTitle;
    ImageButton btnRight1;
    ImageButton btnRight2;
    LinearLayout containerTitlebar1;

    final Handler handler = new Handler();


    public TitleBar(Context context) {
        super(context);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }


    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.titlebar_main, this);
        bindViews();
    }

    private void bindViews() {
        txtTitle = findViewById(R.id.txtTitle);
        btnLeft1 = findViewById(R.id.btnLeft1);
        btnLeft2 = findViewById(R.id.btnLeft2);
        btnRight2 = findViewById(R.id.btnRight2);
        btnRight1 = findViewById(R.id.btnRight1);

        containerTitlebar1 = findViewById(R.id.containerTitlebar1);

    }

    public void resetViews() {
        containerTitlebar1.setVisibility(VISIBLE);
        btnLeft1.setVisibility(GONE);
        btnLeft2.setVisibility(VISIBLE);
        btnRight2.setVisibility(INVISIBLE);
        btnRight1.setVisibility(GONE);

    }

    public TextView getBtnLeft2(){
        return btnLeft2;
    }

    public void setTitle(String title) {

        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(title);
    }

    public void showBackButton(final Activity mActivity) {
        this.btnLeft1.setVisibility(VISIBLE);
        this.btnLeft1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.b_home_icon, 0, 0, 0);
        this.btnLeft1.setText(null);
        btnLeft1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity != null) {
//                    mActivity.getSupportFragmentManager().popBackStack();
                    mActivity.onBackPressed();
                }

            }
        });
    }


    public void setRightButton(int drawable, OnClickListener onClickListener) {
        this.btnRight1.setVisibility(VISIBLE);
        this.btnRight1.setImageResource(drawable);
        this.btnRight1.setOnClickListener(onClickListener);
    }


    public void setRightButton2(int drawable, OnClickListener onClickListener, int color) {
        this.btnRight2.setVisibility(VISIBLE);
        this.btnRight2.setImageResource(drawable);
        this.btnRight2.setOnClickListener(onClickListener);
        this.btnRight2.setColorFilter(color);
    }


    public void showHome(final BaseActivity activity) {
// FIXME: 5/25/2018 txtView--- ImageButton
        this.btnRight2.setVisibility(VISIBLE);
        btnRight2.setImageResource(R.drawable.b_home_icon);
        this.btnRight2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity instanceof HomeActivity) {
//                    activity.reload();
                    activity.popStackTill(1);
//                    activity.notifyToAll(ON_HOME_PRESSED, TitleBar.this);

                } else {
                    activity.clearAllActivitiesExceptThis(HomeActivity.class);
                }
            }
        });
    }



}
