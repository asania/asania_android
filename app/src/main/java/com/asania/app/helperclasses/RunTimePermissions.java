package com.asania.app.helperclasses;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.asania.app.activities.BaseActivity;
import com.asania.app.fragments.dialogs.PermissionDialogFragment;
import com.asania.app.helperclasses.ui.helper.UIHelper;


/**
 * Created by khanhamza on 23-May-17.
 */

public class RunTimePermissions {



    // Storage Permissions variables
    private static final int REQUEST_CODE = 6361;
    private static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.READ_PHONE_STATE
    };



    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int internetPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.INTERNET);
        int wifiPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_WIFI_STATE);
        int readPhonePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);

        if (writePermission != PackageManager.PERMISSION_GRANTED
                || readPermission != PackageManager.PERMISSION_GRANTED
                || cameraPermission != PackageManager.PERMISSION_GRANTED
                || internetPermission != PackageManager.PERMISSION_GRANTED
                || wifiPermission != PackageManager.PERMISSION_GRANTED
                || readPhonePermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS,
                    REQUEST_CODE
            );
        }
    }


    public static boolean isReadPhonePermissionGiven(Context context, BaseActivity activity, boolean showAlert, String alertMessage) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (showAlert) {
//            UIHelper.showAlertDialogSingleButton(alertMessage, "Alert", (dialogInterface, i) -> verifyStoragePermissions(activity), context);
//            GenericDialogFragment genericDialogFragment = GenericDialogFragment.newInstance();

//            UIHelper.showOnlyTextPopup(activity, alertMessage, "Permission Required", true, () -> verifyStoragePermissions(activity));
            UIHelper.showOnlyTextPopup(activity, alertMessage);

        }


        return false;
    }




    public static boolean isAllPhonePermissionGiven(Context context, BaseActivity activity, boolean showAlert, String alertMessage) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }

        if (showAlert) {

            PermissionDialogFragment permissionDialogFragment = PermissionDialogFragment.newInstance();
            permissionDialogFragment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    permissionDialogFragment.dismiss();
                    verifyStoragePermissions(activity);
                }
            });
            permissionDialogFragment.show(activity.getSupportFragmentManager(), PermissionDialogFragment.class.getSimpleName());


        }


        return false;
    }
}
