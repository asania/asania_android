package com.asania.app.enums;

/**
 * Created by aqsa.sarwar on 7/5/2019.
 */

public enum Menu {
    mnuMyPatients,
    mnuFamilyHifazat,
    mnuMyPatientsStatistics,
    mnuMyPatientsMaintenance,
    mnuFamilyHifazatMaintenance,
    mnuFamilyHifazatStatistics;



    public String canonicalForm() {
        return this.name().toLowerCase();
    }

    public static FileType fromCanonicalForm(String canonical) {
        return (FileType) valueOf(FileType.class, canonical.toUpperCase());
    }
}
