package com.asania.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.drawerlayout.widget.DrawerLayout;

import com.asania.R;
import com.asania.app.activities.HomeActivity;
import com.asania.app.constatnts.AppConstants;
import com.asania.app.fragments.abstracts.BaseFragment;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.managers.retrofit.GsonFactory;
import com.asania.app.managers.retrofit.WebServices;
import com.asania.app.models.User.UserModel;
import com.asania.app.models.wrappers.WebResponse;
import com.asania.app.widget.AnyEditTextView;
import com.asania.app.widget.AnyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SignUpFragment extends BaseFragment {
    @BindView(R.id.btnSignIn)
    AnyTextView btnSignIn;
    @BindView(R.id.edtFullName)
    AnyEditTextView edtFullName;
    @BindView(R.id.edtEmailAddress)
    AnyEditTextView edtEmailAddress;
    @BindView(R.id.txtCity)
    AnyTextView txtCity;
    @BindView(R.id.edtPhoneNumber)
    AnyEditTextView edtPhoneNumber;
    @BindView(R.id.edtPassword)
    AnyEditTextView edtPassword;
    @BindView(R.id.chkShowPassword)
    CheckBox chkShowPassword;
    @BindView(R.id.btnSignUp)
    AnyTextView btnSignUp;
    @BindView(R.id.contLogin)
    LinearLayout contLogin;
    Unbinder unbinder;

    public static SignUpFragment newInstance() {

        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getDrawerLockMode() {
        return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_sign_up;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
    }

    @Override
    public void setListeners() {
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnSignIn, R.id.btnSignUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:
                getBaseActivity().replaceDockableFragment(LoginFragment.newInstance(), false);
                break;
            case R.id.btnSignUp:
                signupCall();
               // showNextBuildToast();
                break;
        }
    }
    private void signupCall() {
        if (edtEmailAddress.testValidity() && edtPassword.testValidity()) {
            new WebServices(getBaseActivity(),
                    "",
                    true)
                    .webServiceSignup(edtFullName.getStringTrimmed(),edtEmailAddress.getStringTrimmed(), edtPassword.getStringTrimmed(),edtPhoneNumber.getStringTrimmed(),"Karachi",
                            new WebServices.IRequestWebResponseJustObjectCallBack() {
                                @Override
                                public void requestDataResponse(WebResponse<Object> webResponse) {
                                    UserModel userModel = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.data), UserModel.class);

                                    openHomeActivity(userModel);
                                }

                                @Override
                                public void onError(WebResponse<Object> webResponse) {
                                    UIHelper.showShortToastInCenter(getContext(), "Something went wrong, API error");
                                }
                            });

        }
    }

    private void openHomeActivity( UserModel userModel) {
        sharedPreferenceManager.putObject(AppConstants.KEY_CURRENT_USER_MODEL, userModel);
        getBaseActivity().openActivity(HomeActivity.class);
        getBaseActivity().finish();
    }

}
