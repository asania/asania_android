package com.asania.app.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;

import com.asania.R;
import com.asania.app.activities.ActivityProfile;
import com.asania.app.activities.ActivitySettings;
import com.asania.app.activities.HomeActivity;
import com.asania.app.activities.MainActivity;
import com.asania.app.fragments.abstracts.BaseFragment;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.widget.AnyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by khanhamza on 09-May-17.
 */

public class LeftSideMenuFragment extends BaseFragment  {

    Unbinder unbinder;
    @BindView(R.id.txtHome)
    AnyTextView txtHome;
    @BindView(R.id.txtCardSubscription)
    AnyTextView txtCardSubscription;
    @BindView(R.id.txtAbout)
    AnyTextView txtAbout;
    @BindView(R.id.txtSettings)
    AnyTextView txtSettings;
    @BindView(R.id.txtLogout)
    AnyTextView txtLogout;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.imgBackground)
    ImageView imgBackground;

    public static LeftSideMenuFragment newInstance() {
        Bundle args = new Bundle();

        LeftSideMenuFragment fragment = new LeftSideMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_sidebar;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ////        scrollToTop();
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public int getDrawerLockMode() {
        return DrawerLayout.LOCK_MODE_UNDEFINED;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    public static void logoutClick(final BaseFragment baseFragment) {
        Context context = baseFragment.getContext();

//        new iOSDialogBuilder(context)
//                .setTitle(context.getString(R.string.logout))
//                .setSubtitle(context.getString(R.string.areYouSureToLogout))
//                .setBoldPositiveLabel(false)
//                .setCancelable(false)
//                .setPositiveListener(context.getString(R.string.yes), dialog -> {
//                    dialog.dismiss();
//                    baseFragment.sharedPreferenceManager.clearDB();
//                    baseFragment.getBaseActivity().clearAllActivitiesExceptThis(MainActivity.class);
//                })
//                .setNegativeListener(context.getString(R.string.no), dialog -> dialog.dismiss())
//                .build().show();
//


        UIHelper.showAlertDialog(context.getString(R.string.areYouSureToLogout), context.getString(R.string.logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                baseFragment.sharedPreferenceManager.clearDB();
                baseFragment.getBaseActivity().clearAllActivitiesExceptThis(MainActivity.class);
            }
        }, context);


    }

    public void scrollToTop() {
        scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(0, 0);
//                scrollView.fullScroll(View.FOCUS_UP);
            }
        });
    }


    @OnClick({ R.id.txtHome, R.id.txtCardSubscription, R.id.txtAbout, R.id.txtLogout, R.id.txtSettings,R.id.txtProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtHome:
                if (getActivity() instanceof HomeActivity) {
                    getBaseActivity().reload();
                } else {
                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                }
                break;

            case R.id.txtCardSubscription:
//                getBaseActivity().addDockableFragment(CardSubscriptionFragment.newInstance());
                showNextBuildToast();
                break;
            case R.id.txtSettings:
                startActivity(new Intent(getContext(), ActivitySettings.class));

            case R.id.txtAbout:
                break;
            case R.id.txtProfile:
                startActivity(new Intent(getContext(), ActivityProfile.class));
                break;
            case R.id.txtLogout:
                logoutClick(this);
                break;
        }
    }


}
