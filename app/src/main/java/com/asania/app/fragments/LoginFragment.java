package com.asania.app.fragments;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;

import com.asania.R;
import com.asania.app.activities.HomeActivity;
import com.asania.app.constatnts.AppConstants;
import com.asania.app.fragments.abstracts.BaseFragment;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.managers.retrofit.GsonFactory;
import com.asania.app.managers.retrofit.WebServices;
import com.asania.app.models.User.UserModel;
import com.asania.app.models.wrappers.WebResponse;
import com.asania.app.widget.AnyEditTextView;
import com.asania.app.widget.AnyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class LoginFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.btnSignUp)
    AnyTextView btnSignUp;
    @BindView(R.id.edtEmailAddress)
    AnyEditTextView edtEmailAddress;
    @BindView(R.id.edtPassword)
    AnyEditTextView edtPassword;
    @BindView(R.id.txtForgotPassword)
    AnyTextView txtForgotPassword;
    @BindView(R.id.chkShowPassword)
    CheckBox chkShowPassword;
    @BindView(R.id.btnLogin)
    AnyTextView btnLogin;
    @BindView(R.id.contContent)
    LinearLayout contContent;
    @BindView(R.id.btnfb)
    ImageView btnfb;
    @BindView(R.id.btnGmail)
    ImageView btnGmail;
    @BindView(R.id.contLogin)
    LinearLayout contLogin;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_login;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
    }


    @Override
    public void setListeners() {
        chkShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    edtPassword.setTransformationMethod(null);
                } else {
                    edtPassword.setTransformationMethod(new PasswordTransformationMethod());
                }
                edtPassword.setSelection(edtPassword.getText().length());
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public int getDrawerLockMode() {
        return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onClick(View v) {

    }


//    @OnClick(R.id.btnLogin)
//    public void onViewClicked() {
//
//
//    }

    private void loginCall() {
        if (edtEmailAddress.testValidity() && edtPassword.testValidity()) {
            openHomeActivity(null);
           /* new WebServices(getBaseActivity(),
                    "",
                    true)
                    .webServiceLogin(*//*edtEmailAddress*//*"abiha@g.com".trim()*//*.getStringTrimmed()*//*,"123456".trim() *//*edtPassword.getStringTrimmed()*//*,
                            new WebServices.IRequestWebResponseJustObjectCallBack() {
                                @Override
                                public void requestDataResponse(WebResponse<Object> webResponse) {
                                    UserModel userModel = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.data), UserModel.class);

                                    openHomeActivity(userModel);
                                }

                                @Override
                                public void onError(WebResponse<Object> webResponse) {
                                    UIHelper.showShortToastInCenter(getContext(), "Something went wrong, API error");
                                }
                            });*/

        }
    }

    private void openHomeActivity( UserModel userModel) {
        sharedPreferenceManager.putObject(AppConstants.KEY_CURRENT_USER_MODEL, userModel);
        getBaseActivity().openActivity(HomeActivity.class);
        getBaseActivity().finish();
    }

    @OnClick({R.id.btnSignUp, R.id.txtForgotPassword, R.id.btnLogin, R.id.btnfb, R.id.btnGmail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                getBaseActivity().replaceDockableFragment(SignUpFragment.newInstance(), false);
                break;
            case R.id.txtForgotPassword:
                getBaseActivity().replaceDockableFragment(ForgotPasswordFragment.newInstance(), false);

                break;
            case R.id.btnLogin:
                loginCall();
                break;
            case R.id.btnfb:
            case R.id.btnGmail:
                showNextBuildToast();
                break;
        }
    }
}
