package com.asania.app.fragments.dialogs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.asania.R;
import com.asania.app.widget.AnyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by khanhamza on 21-Feb-17.
 */

public class PermissionDialogFragment extends DialogFragment {


    Unbinder unbinder;
    @BindView(R.id.txtPermission)
    AnyTextView txtPermission;
    @BindView(R.id.btnOK)
    AnyTextView btnOK;
    private View.OnClickListener onClickListener;


    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public PermissionDialogFragment() {
    }

    public static PermissionDialogFragment newInstance() {
        PermissionDialogFragment frag = new PermissionDialogFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_permissions, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

    }


    @OnClick(R.id.btnOK)
    public void onViewClicked() {
        if (onClickListener != null) {
            onClickListener.onClick(null);
        }
    }
}

