package com.asania.app.fragments.abstracts;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.gdacciaro.iOSDialog.iOSDialogBuilder;

import com.asania.BaseApplication;

import com.asania.R;
import com.asania.app.activities.BaseActivity;
import com.asania.app.activities.MainActivity;
import com.asania.app.constatnts.AppConstants;
import com.asania.app.helperclasses.ui.helper.KeyboardHelper;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.managers.SharedPreferenceManager;
import com.asania.app.models.User.UserModel;
import io.reactivex.disposables.Disposable;

public abstract class BaseFragment extends Fragment implements
        View.OnClickListener, AdapterView.OnItemClickListener/*, OnNewPacketReceivedListener */ {

    protected View view;
    public SharedPreferenceManager sharedPreferenceManager;
    public String TAG = "Logging Tag";
    public boolean onCreated = false;
    Disposable subscription;

    /**
     * This is an abstract class, we should inherit our fragment from this class
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(getContext());
        onCreated = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBaseActivity().getTitleBar().resetViews();
        getBaseActivity().getDrawerLayout().setDrawerLockMode(getDrawerLockMode());
        getBaseActivity().getDrawerLayout().closeDrawer(Gravity.LEFT);


    }

    public UserModel getCurrentUser() {
        return sharedPreferenceManager.getCurrentUser();
    }

    public abstract int getDrawerLockMode();

    // Use  UIHelper.showSpinnerDialog
    @Deprecated
    public void setSpinner(ArrayAdapter adaptSpinner, final TextView textView, final Spinner spinner) {
        if (adaptSpinner == null || spinner == null)
            return;
        //selected item will look like a spinner set from XML
//        simple_list_item_single_choice
        adaptSpinner.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adaptSpinner);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = spinner.getItemAtPosition(position).toString();
                if (textView != null)
                    textView.setText(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    protected abstract int getFragmentLayout();

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }


    public void emptyBackStack() {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popBackStack() {
        if (getFragmentManager() == null) {
            return;
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void popStackTill(int stackNumber) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = stackNumber; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popStackTillReverse(int stackNumber) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < stackNumber; i++) {
            fm.popBackStack();
        }
    }

    public abstract void setTitlebar(TitleBar titleBar);


    public abstract void setListeners();

    @Override
    public void onResume() {
        super.onResume();
        onCreated = true;
        setListeners();

        if (getBaseActivity() != null) {
            setTitlebar(getBaseActivity().getTitleBar());
        }
//
        if (getBaseActivity() != null && getBaseActivity().getWindow().getDecorView() != null) {
            KeyboardHelper.hideSoftKeyboard(getBaseActivity(), getBaseActivity().getWindow().getDecorView());
        }
    }

    @Override
    public void onPause() {

        if (getBaseActivity() != null && getBaseActivity().getWindow().getDecorView() != null) {
            KeyboardHelper.hideSoftKeyboard(getBaseActivity(), getBaseActivity().getWindow().getDecorView());
        }
        super.onPause();
    }


    public void notifyToAll(int event, Object data) {
        BaseApplication.getPublishSubject().onNext(new Pair<>(event, data));
    }

//    protected void subscribeToNewPacket(final OnNewPacketReceivedListener newPacketReceivedListener) {
//        subscription = BaseApplication.getPublishSubject()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Pair>() {
//                    @Override
//                    public void accept(@NonNull Pair pair) throws Exception {
//                        Log.e("abc", "on accept");
//                        newPacketReceivedListener.onNewPacket((int) pair.first, pair.second);
//                    }
//                });
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("abc", "onDestroyView");
        if (subscription != null)
            subscription.dispose();
    }


    public void showNextBuildToast() {
        UIHelper.showToast(getContext(), "This feature is in progress");
    }

    public static void logoutClick(final BaseFragment baseFragment) {
        Context context = baseFragment.getContext();

        new iOSDialogBuilder(context)
                .setTitle(context.getString(R.string.logout))
                .setSubtitle(context.getString(R.string.areYouSureToLogout))
                .setBoldPositiveLabel(false)
                .setCancelable(false)
                .setPositiveListener(context.getString(R.string.yes), dialog -> {
                    dialog.dismiss();
//                    baseFragment.sharedPreferenceManager.clearDB();
                    baseFragment.getBaseActivity().clearAllActivitiesExceptThis(MainActivity.class);
                })
                .setNegativeListener(context.getString(R.string.no), dialog -> dialog.dismiss())
                .build().show();


    }


    public void putOneTimeToken(String token) {
        sharedPreferenceManager.putValue(AppConstants.KEY_ONE_TIME_TOKEN, token);
    }

//    @Override
//    public void onNewPacket(int event, Object data) {
//        switch (event) {
//            case ON_SELECTED_USER_UPDATE:
//                if (data instanceof UserModel) {
//                    UserModel userDetailModel = (UserModel) data;
//                    if (getBaseActivity().getTitleBar() != null
//                            && getBaseActivity().getTitleBar().circleImageView != null
//                            && getBaseActivity().getTitleBar().circleImageView.getVisibility() == View.VISIBLE) {
//                        getBaseActivity().getTitleBar().setUserDisplay(userDetailModel, getContext());
//                    }
//                }
//                break;
//        }
//    }
}

