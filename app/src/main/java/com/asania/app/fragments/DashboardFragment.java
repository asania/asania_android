package com.asania.app.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.asania.R;
import com.asania.app.activities.MainActivity;
import com.asania.app.adapters.ViewPagerAdapter;
import com.asania.app.fragments.abstracts.BaseFragment;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.widget.AnyTextView;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class DashboardFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    TitleBar titleBar;

    ViewPagerAdapter viewPagerAdapter;
    private int[] tabIcons = {  R.drawable.marques,
            R.drawable.buffets,R.drawable.planners,R.drawable.tour
    };
    //
    public static DashboardFragment newInstance() {
        Bundle args = new Bundle();
        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public int getDrawerLockMode() {
        return DrawerLayout.LOCK_MODE_UNLOCKED;
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.setTitle("Marques");
        titleBar.showBackButton(getBaseActivity());

        titleBar.setRightButton2(R.drawable.b_home_icon, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.showAlertDialog(getResources().getString(R.string.areYouSureToLogout), "Logout",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sharedPreferenceManager.clearDB();
                                getBaseActivity().clearAllActivitiesExceptThis(MainActivity.class);
                            }
                        }
                        , getContext());
            }
        }, getResources().getColor(R.color.c_white));
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        titleBar = (TitleBar) getActivity().findViewById(R.id.titlebar);
        titleBar.setTitle("Marques");
        initViewPager();
        return rootView;
    }

    public void initViewPager(){
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragment(HomeFragment.newInstance());
        viewPagerAdapter.addFragment(HomeFragment.newInstance());
        viewPagerAdapter.addFragment(HomeFragment.newInstance());
        viewPagerAdapter.addFragment(TourFragment.newInstance());

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.addTab(tabLayout.newTab().setText("Marques"));
        tabLayout.addTab(tabLayout.newTab().setText("Buffets"));
        tabLayout.addTab(tabLayout.newTab().setText("Planner"));
        tabLayout.addTab(tabLayout.newTab().setText("Tour Package"));
        onTabchange();
        setupTabIcons();
        tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
    }

    private void onTabchange(){
        tabLayout.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                    viewPager.setCurrentItem(tab.getPosition());
                    if(tab.getPosition()==0)
                        titleBar.setTitle("Marques");
                    else if(tab.getPosition()==1)
                        titleBar.setTitle("Buffets");
                    else if(tab.getPosition()==2)
                        titleBar.setTitle("Planner");
                }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(getResources().getColor(R.color.material_amber700), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);

        tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.material_amber700), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.material_amber700), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).getIcon().setColorFilter(getResources().getColor(R.color.material_amber700), PorterDuff.Mode.SRC_IN);

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}