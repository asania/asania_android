package com.asania.app.managers.retrofit;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;

import com.asania.app.helperclasses.Helper;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.models.wrappers.WebResponse;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by hamzakhan on 6/30/2017.
 */

public class WebServices {
    private WebServiceProxy apiService;
    private KProgressHUD mDialog;
    private Context mContext;
    private static String bearerToken = "";

    public static String getBearerToken() {
        return bearerToken;
    }

    public static void setBearerToken(String bearerToken) {
        WebServices.bearerToken = bearerToken;
    }


    public WebServices(Activity activity, String token, boolean isShowDialog) {
        apiService = WebServiceFactory.getInstance(token);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }


    // For New Token
    public WebServices(Activity activity, String newToken, boolean enableReferesh, boolean isShowDialog) {
        apiService = WebServiceFactory.getInstance(newToken, enableReferesh);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }


    /**
     * This API is for Any type of Object result but under our WebResponse wrapper
     * <p>
     * "ResponseMessage": "",
     * "ResponseCode": 200,   // You will get 200 is there is any data or 204 is no data is found
     * "ResponseType": "UserManager.GetUserPreferenceUsingMapper",
     * "ConsultationHistoryModel": []
     *
     * @param requestMethod
     * @param requestData
     * @param callBack
     * @return
     */

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObject(String requestMethod, String requestData, final IRequestWebResponseAnyObjectCallBack callBack) {
        RequestBody bodyRequestMethod = getRequestBody(MultipartBody.FORM, requestMethod);
        RequestBody bodyRequestData = getRequestBody(MultipartBody.FORM, requestData);
        Call<WebResponse<Object>> webResponseCall = apiService.webServiceRequestAPIForWebResponseAnyObject(bodyRequestMethod, bodyRequestData);

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.body().status==200) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } /*else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        }*/ else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }


    @NonNull
    private RequestBody getRequestBody(MediaType form, String trim) {
        return RequestBody.create(
                form, trim);
    }


    private void dismissDialog() {
        mDialog.dismiss();
    }


    public interface IRequestWebResponseAnyObjectCallBack {
        void requestDataResponse(WebResponse<Object> webResponse);

        void onError(Object object);
    }

    public interface IRequestJustObjectCallBack {
        void requestDataResponse(Object webResponse);

        void onError(Object object);
    }

    public interface IRequestJsonDataCallBack {
        void requestDataResponse(WebResponse<JsonObject> webResponse);

        void onError();
    }


    public void webServiceLogin(String email, String password, IRequestWebResponseJustObjectCallBack iRequestWebResponseJustObjectCallBack) {
//        Call<WebResponse<Object>> method = apiService.login(email, password, "test", "android");
        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                WebServiceFactory.getInstance("").
                        login(email, password, "test", "android")
                        .enqueue(new Callback<WebResponse<Object>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                                dismissDialog();
                                if (response.body() == null) {
                                    iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
                                } else {
                                    iRequestWebResponseJustObjectCallBack.requestDataResponse(response.body());
                                }
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                                t.printStackTrace();
                                iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
                                UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                                dismissDialog();

                            }
                        });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

    }


    public void webServiceSignup(String name,String email, String password,String phone, String city, IRequestWebResponseJustObjectCallBack iRequestWebResponseJustObjectCallBack) {
//        Call<WebResponse<Object>> method = apiService.login(email, password, "test", "android");
        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                WebServiceFactory.getInstance("").
                        register(name,email, password, phone, city)
                        .enqueue(new Callback<WebResponse<Object>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                                dismissDialog();
                                if (response.body() == null) {
                                    iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
                                } else {
                                    iRequestWebResponseJustObjectCallBack.requestDataResponse(response.body());
                                }
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                                iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
                                UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                                dismissDialog();

                            }
                        });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

    }

    public interface IRequestWebResponseJustObjectCallBack {
        void requestDataResponse(WebResponse<Object> webResponse);

        void onError(WebResponse<Object> webResponse);
    }


}


