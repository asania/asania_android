package com.asania.app.managers.retrofit;


import com.asania.app.constatnts.WebServiceConstants;
import com.asania.app.models.wrappers.WebResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by khanhamza on 09-Mar-17.
 */

public interface WebServiceProxy {


     @Multipart
    @POST("./")
    Call<WebResponse<Object>> webServiceRequestAPIForWebResponseAnyObject(
             @Part(WebServiceConstants.PARAMS_REQUEST_METHOD) RequestBody requestMethod,
             @Part(WebServiceConstants.PARAMS_REQUEST_DATA) RequestBody requestData
     );



    @Multipart
    @POST("./")
    Call<WebResponse<Object>> uploadFileRequestApi(
            @Part(WebServiceConstants.PARAMS_REQUEST_METHOD) RequestBody requestMethod,
            @Part MultipartBody.Part requestData
    );


    @Headers(WebServiceConstants.WS_KEY_GET_REQUESTOR)
    @GET(WebServiceConstants.WS_KEY_GET_TOKEN)
    Call<String> getToken();


    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_REGISTER)
    Call<WebResponse<Object>> register(
            @Query("name") String name,
            @Query("email") String email,
            @Query("password") String password,
            @Query("phone") String phone,
            @Query("city") String city
    );



    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_LOGIN)
    Call<WebResponse<Object>> login(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
    );

    @GET(WebServiceConstants.WS_KEY_AUTHENTICATE_USER)
    Call<Object> getAuthenticatedUserDetails(
            @Query("UserName") String userName,
            @Query("Application") String application
    );



}

