package com.asania.app.libraries.searchdialog.core;

public interface Searchable {
    String getTitle();

    String getContent();

    String getSearchTime();
}
