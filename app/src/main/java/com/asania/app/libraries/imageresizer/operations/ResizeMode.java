package com.asania.app.libraries.imageresizer.operations;

public enum ResizeMode {

	AUTOMATIC,
	FIT_TO_WIDTH,
	FIT_TO_HEIGHT,
	FIT_EXACT
	
}
