package com.asania.app.libraries.imageloader;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.asania.R;


/**
 * Created by khanhamza on 08-Mar-17.
 */

public class LazyLoading {

    public static DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
            .showImageForEmptyUri(R.color.material_grey100)
            .showImageOnFail(R.drawable.profile_placeholder)
            .showImageOnLoading(R.drawable.profile_placeholder).build();
}
