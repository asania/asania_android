package com.asania.app.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllApplicationMenuList implements Serializable
{

@SerializedName("ApplicationRole")
@Expose
private String applicationRole;
@SerializedName("ApplicationMenu")
@Expose
private String applicationMenu;
@SerializedName("Parent")
@Expose
private String parent;
@SerializedName("Order")
@Expose
private Integer order;
private final static long serialVersionUID = -7113408084146334254L;

public String getApplicationRole() {
return applicationRole;
}

public void setApplicationRole(String applicationRole) {
this.applicationRole = applicationRole;
}

public String getApplicationMenu() {
return applicationMenu;
}

public void setApplicationMenu(String applicationMenu) {
this.applicationMenu = applicationMenu;
}

public String getParent() {
return parent;
}

public void setParent(String parent) {
this.parent = parent;
}

public Integer getOrder() {
return order;
}

public void setOrder(Integer order) {
this.order = order;
}

}