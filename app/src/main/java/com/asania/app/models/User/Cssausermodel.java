package com.asania.app.models.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.asania.app.managers.retrofit.GsonFactory;

public class Cssausermodel {

    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("Role")
    @Expose
    private String role;
    @SerializedName("RoleDescription")
    @Expose
    private String roleDescription;
    @SerializedName("FacilityID")
    @Expose
    private String facilityID;
    @SerializedName("FacilityDescription")
    @Expose
    private String facilityDescription;
    @SerializedName("ApplicationID")
    @Expose
    private String applicationID;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Mnemonic")
    @Expose
    private String mnemonic;
    @SerializedName("Speciality")
    @Expose
    private String speciality;
    @SerializedName("PhysicianNumber")
    @Expose
    private String physicianNumber;
    @SerializedName("UserPictureExists")
    @Expose
    private Boolean userPictureExists;
    @SerializedName("UserPicture")
    @Expose
    private String userPicture;
    @SerializedName("PIN")
    @Expose
    private String pIN;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("AllowAccessToNewApp")
    @Expose
    private String AllowAccessToNewApp;
    private final static long serialVersionUID = -3488093837490723318L;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getFacilityID() {
        return facilityID;
    }

    public void setFacilityID(String facilityID) {
        this.facilityID = facilityID;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public String getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(String applicationID) {
        this.applicationID = applicationID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getPhysicianNumber() {
        return physicianNumber;
    }

    public void setPhysicianNumber(String physicianNumber) {
        this.physicianNumber = physicianNumber;
    }

    public Boolean getUserPictureExists() {
        return userPictureExists;
    }

    public void setUserPictureExists(Boolean userPictureExists) {
        this.userPictureExists = userPictureExists;
    }

    public String getUserPicture() {

        if (this.userPictureExists) {
            return userPicture;
        } else {
            return "";
        }

//        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public String getPIN() {
        return pIN;
    }

    public void setPIN(String pIN) {
        this.pIN = pIN;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAllowAccessToNewApp() {
        return AllowAccessToNewApp;
    }

    public void setAllowAccessToNewApp(String allowAccessToNewApp) {
        AllowAccessToNewApp = allowAccessToNewApp;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}
