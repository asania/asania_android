package com.asania.app.models.sendingmodels;

import com.asania.app.managers.retrofit.GsonFactory;
import com.google.gson.annotations.SerializedName;


/**
 * Created by aqsa.sarwar on 1/2/2018.
 */

public class LoginApiModel {

    @SerializedName("email")
    private String email;
    @SerializedName("Password")
    private String password;
    @SerializedName("device_type")
    private String device_type;
    @SerializedName("device_token")
    private String device_token;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }
}
