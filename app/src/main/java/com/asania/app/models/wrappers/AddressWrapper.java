package com.asania.app.models.wrappers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.asania.app.models.extramodels.AddressModel;

/**
 * Created by khanhamza on 11-Mar-17.
 */

public class AddressWrapper {
    @SerializedName("Address")
    public ArrayList<AddressModel> addresses;

}
