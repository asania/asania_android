package com.asania.app.models.receivingmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.asania.app.managers.retrofit.GsonFactory;


public class UserDetailModel {
    transient boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @SerializedName("Role")
    @Expose
    private String Role;
    @SerializedName("LoginCount")
    @Expose
    private String Count;

    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("UserID")
    @Expose
    private String UserId;
    @SerializedName("CompleteName")
    @Expose
    private String CompleteName;
    private String formattedName;

    @SerializedName("Designation")
    @Expose
    private String Designation;
    @SerializedName("_token")
    @Expose
    private String token;
    @SerializedName("MemberURN")
    @Expose
    private String memberURN;
    @SerializedName("MembershipTypeID")
    @Expose
    private String membershipTypeID;
    @SerializedName("MembershipTypeDescription")
    @Expose
    private String membershipTypeDescription;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("BirthDate")
    @Expose
    private String birthDate;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("GenderDescription")
    @Expose
    private String genderDescription;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("RelationshipID")
    @Expose
    private String relationshipID;
    @SerializedName("RelationshipDescription")
    @Expose
    private String relationshipDescription;
    @SerializedName("OtherRelationshipID")
    @Expose
    private String otherRelationshipID;
    @SerializedName("NationalityID")
    @Expose
    private String nationalityID;
    @SerializedName("NationalityDescription")
    @Expose
    private String nationalityDescription;
    @SerializedName("NationalityTypeID")
    @Expose
    private String nationalityTypeID;
    @SerializedName("NationalityTypeDescription")
    @Expose
    private String nationalityTypeDescription;
    @SerializedName("CNICNumber")
    @Expose
    private String cNICNumber;
    @SerializedName("PassportNumber")
    @Expose
    private String passportNumber;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("CellPhoneNumber")
    @Expose
    private String cellPhoneNumber;
    @SerializedName("LandlineNumber")
    @Expose
    private String landlineNumber;
    @SerializedName("CurrentAddress")
    @Expose
    private String currentAddress;
    @SerializedName("CurrentCity")
    @Expose
    private String currentCity;
    @SerializedName("CurrentCountryID")
    @Expose
    private String currentCountryID;
    @SerializedName("CurrentCountryDescription")
    @Expose
    private String currentCountryDescription;
    @SerializedName("CompleteCurrentAddress")
    @Expose
    private String completeCurrentAddress;
    @SerializedName("PermanentAddress")
    @Expose
    private String permanentAddress;
    @SerializedName("PermanentCity")
    @Expose
    private String permanentCity;
    @SerializedName("PermanentCountryID")
    @Expose
    private String permanentCountryID;
    @SerializedName("PermanentCountryDescription")
    @Expose
    private String permanentCountryDescription;
    @SerializedName("CompletePermanentAddress")
    @Expose
    private String completePermanentAddress;
    @SerializedName("MRNumber")
    @Expose
    private String mRNumber;
    @SerializedName("NewMRNumber")
    @Expose
    private String newMRNumber;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("ProfileImagePath")
    @Expose
    private String profileImagePath;
    @SerializedName("SequenceNumber")
    @Expose
    private String sequenceNumber;
    @SerializedName("AllowAccToPortal")
    @Expose
    private String allowAccToPortal;
    @SerializedName("AllowAccToPrivilege")
    @Expose
    private String allowAccToPrivilege;
    @SerializedName("strBirthDate")
    @Expose
    private String strBirthDate;
    @SerializedName("AttachmentList")
    @Expose
    private String attachmentList;
    @SerializedName("LastFileDateTime")
    @Expose
    private String lastFileDateTime;
    @SerializedName("LastFileUser")
    @Expose
    private String lastFileUser;
    @SerializedName("LastFileTerminal")
    @Expose
    private String lastFileTerminal;
    @SerializedName("Active")
    @Expose
    private String active;
    @SerializedName("RecordFound")
    @Expose
    private String recordFound;
    @SerializedName("RecordMessage")
    @Expose
    private String recordMessage;

    @SerializedName("IsAccToPortalAllowed")
    @Expose
    private boolean isAccToPortalAllowed;

    @SerializedName("IsAccToPrivilegeAllowed")
    @Expose
    private boolean isAccToPrivilegeAllowed;

    @SerializedName("RequestStatusDescription")
    @Expose
    private String RequestStatusDescription;
    @SerializedName("RequestedDateTime")
    @Expose
    private String requestedDateTime;

    public boolean isAccToPortalAllowed() {
        return isAccToPortalAllowed;
    }

    public void setAccToPortalAllowed(boolean accToPortalAllowed) {
        isAccToPortalAllowed = accToPortalAllowed;
    }

    public boolean isAccToPrivilegeAllowed() {
        return isAccToPrivilegeAllowed;
    }

    public void setAccToPrivilegeAllowed(boolean accToPrivilegeAllowed) {
        isAccToPrivilegeAllowed = accToPrivilegeAllowed;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMemberURN() {
        return memberURN;
    }

    public void setMemberURN(String memberURN) {
        this.memberURN = memberURN;
    }

    public String getMembershipTypeID() {
        return membershipTypeID;
    }

    public void setMembershipTypeID(String membershipTypeID) {
        this.membershipTypeID = membershipTypeID;
    }

    public String getMembershipTypeDescription() {
        return membershipTypeDescription;
    }

    public void setMembershipTypeDescription(String membershipTypeDescription) {
        this.membershipTypeDescription = membershipTypeDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }



    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getFormattedName() {
        return formattedName;
    }

    public void setFormattedName(String formattedName) {
        this.formattedName = formattedName;
    }


    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getCompleteName() {
        return CompleteName;
    }


    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}