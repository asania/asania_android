package com.asania.app.models;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.asania.app.managers.retrofit.GsonFactory;

public class ApplicationRoleList implements Serializable {

    @SerializedName("ApplicationRole")
    @Expose
    private String applicationRole;
    @SerializedName("ApplicationMenu")
    @Expose
    private Object applicationMenu;
    @SerializedName("Parent")
    @Expose
    private Object parent;
    @SerializedName("Order")
    @Expose
    private Integer order;
    private final static long serialVersionUID = -4718322746811193589L;

    public String getApplicationRole() {
        return applicationRole;
    }

    public void setApplicationRole(String applicationRole) {
        this.applicationRole = applicationRole;
    }

    public Object getApplicationMenu() {
        return applicationMenu;
    }

    public void setApplicationMenu(Object applicationMenu) {
        this.applicationMenu = applicationMenu;
    }

    public Object getParent() {
        return parent;
    }

    public void setParent(Object parent) {
        this.parent = parent;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}