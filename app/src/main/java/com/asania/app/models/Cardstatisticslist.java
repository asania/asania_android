package com.asania.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cardstatisticslist {
    @SerializedName("Key")
    @Expose
    private String key;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Result")
    @Expose
    private DevicesResult result;
    private final static long serialVersionUID = 7439213273077870046L;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DevicesResult getResult() {
        return result;
    }

    public void setResult(DevicesResult result) {
        this.result = result;
    }
}
