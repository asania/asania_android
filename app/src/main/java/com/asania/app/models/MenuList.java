package com.asania.app.models;

/**
 * Created by aqsa.sarwar on 7/5/2019.
 */

public class MenuList {

    private String applicationRole;
    private String key;
    private String applicationMenu;
    private int images;
    private int color;

    public MenuList(String applicationRole, String key, String applicationMenu, int images, int color) {
        this.applicationRole = applicationRole;
        this.key = key;
        this.applicationMenu = applicationMenu;
        this.images = images;
        this.color = color;
    }

    public String getApplicationRole() {
        return applicationRole;
    }

    public void setApplicationRole(String applicationRole) {
        this.applicationRole = applicationRole;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getApplicationMenu() {
        return applicationMenu;
    }

    public void setApplicationMenu(String applicationMenu) {
        this.applicationMenu = applicationMenu;
    }

    public int getImages() {
        return images;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
