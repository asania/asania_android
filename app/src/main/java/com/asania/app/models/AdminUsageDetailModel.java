package com.asania.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hamza.ahmed on 8/6/2018.
 */

public class AdminUsageDetailModel {


    @Expose
    @SerializedName("AccessToDateTime")
    private String accesstodatetime;
    @Expose
    @SerializedName("AccessFromDateTime")
    private String accessfromdatetime;
    @Expose
    @SerializedName("RecordMessage")
    private String recordmessage;
    @Expose
    @SerializedName("RecordFound")
    private String recordfound;
    @Expose
    @SerializedName("AppVersion")
    private String appversion;
    @Expose
    @SerializedName("LoginCode")
    private String logincode;
    @Expose
    @SerializedName("ISSUCCESSFUL")
    private String issuccessful;
    @Expose
    @SerializedName("AccessDTTM")
    private String accessdatetime;
    @Expose
    @SerializedName("DeviceOS")
    private String deviceos;
    @Expose
    @SerializedName("RegCardNumber")
    private String regcardnumber;
    @Expose
    @SerializedName("DeviceId")
    private String deviceid;
    @Expose
    @SerializedName("URN")
    private int urn;


    public String getAccesstodatetime() {
        return accesstodatetime;
    }

    public void setAccesstodatetime(String accesstodatetime) {
        this.accesstodatetime = accesstodatetime;
    }

    public String getAccessfromdatetime() {
        return accessfromdatetime;
    }

    public void setAccessfromdatetime(String accessfromdatetime) {
        this.accessfromdatetime = accessfromdatetime;
    }

    public String getRecordmessage() {
        return recordmessage;
    }

    public void setRecordmessage(String recordmessage) {
        this.recordmessage = recordmessage;
    }

    public String getRecordfound() {
        return recordfound;
    }

    public void setRecordfound(String recordfound) {
        this.recordfound = recordfound;
    }

    public String getAppversion() {
        return appversion;
    }

    public void setAppversion(String appversion) {
        this.appversion = appversion;
    }

    public String getLogincode() {
        return logincode;
    }

    public void setLogincode(String logincode) {
        this.logincode = logincode;
    }

    public String getIssuccessful() {
        return issuccessful;
    }

    public void setIssuccessful(String issuccessful) {
        this.issuccessful = issuccessful;
    }

    public String getAccessdatetime() {
        return accessdatetime;
    }

    public void setAccessdatetime(String accessdatetime) {
        this.accessdatetime = accessdatetime;
    }

    public String getDeviceos() {
        return deviceos;
    }

    public void setDeviceos(String deviceos) {
        this.deviceos = deviceos;
    }

    public String getRegcardnumber() {
        return regcardnumber;
    }

    public void setRegcardnumber(String regcardnumber) {
        this.regcardnumber = regcardnumber;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public int getUrn() {
        return urn;
    }

    public void setUrn(int urn) {
        this.urn = urn;
    }
}
