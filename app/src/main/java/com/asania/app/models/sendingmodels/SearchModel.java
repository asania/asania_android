package com.asania.app.models.sendingmodels;

import com.asania.app.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchModel {

    @Expose
    @SerializedName("QuestionID")
    private String QuestionID;
    @Expose
    @SerializedName("Question")
    private String Question;



    @Expose
    @SerializedName("VisitID")
    private String VisitID;
    @Expose
    @SerializedName("URN")
    private String URN;
    @Expose
    @SerializedName("DeviceID")
    private String deviceid;
    @Expose
    @SerializedName("UserID")
    private String userID;

    @Expose
    @SerializedName("RXNUMBER")
    private String rxnumber;

    @Expose
    @SerializedName("TestID")
    private String TestID;

    @Expose
    @SerializedName("DoctorID")
    private String DoctorID;

    @Expose
    @SerializedName("MRNumber")
    private String MRNumber;

    @Expose
    @SerializedName("LocationID")
    private String LocationID;

    @Expose
    @SerializedName("SpecimenNumber")
    private String specimenNumber;

    @Expose
    @SerializedName("IsPatientExists")
    private boolean IsPatientExists;

    @Expose
    @SerializedName("DrugType")
    private String drugType;

    @Expose
    @SerializedName("DoseDateTime")
    private String doseDateTime;

    @Expose
    @SerializedName("ReviewedBy")
    private String ReviewedBy;

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }


    public String getReviewedBy() {
        return ReviewedBy;
    }

    public void setReviewedBy(String reviewedBy) {
        ReviewedBy = reviewedBy;
    }

    public String getURN() {
        return URN;
    }

    public void setURN(String URN) {
        this.URN = URN;
    }

    public String getDoseDateTime() {
        return doseDateTime;
    }

    public void setDoseDateTime(String doseDateTime) {
        this.doseDateTime = doseDateTime;
    }

//    public String getFacilityID() {
//        return FacilityID;
//    }
//
//    public void setFacilityID(String facilityID) {
//        FacilityID = facilityID;
//    }

    public String getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(String doctorID) {
        DoctorID = doctorID;
    }

    public String getRxnumber() {
        return rxnumber;
    }

    public void setRxnumber(String rxnumber) {
        this.rxnumber = rxnumber;
    }

    public boolean getIsPatientExists() {
        return IsPatientExists;
    }

    public void setIsPatientExists(boolean isPatientExists) {
        IsPatientExists = isPatientExists;
    }

    public String getSpecimenNumber() {
        return specimenNumber;
    }

    public void setSpecimenNumber(String specimenNumber) {
        this.specimenNumber = specimenNumber;
    }

    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String VisitID) {
        this.VisitID = VisitID;
    }

    public String getTestID() {
        return TestID;
    }

    public void setTestID(String testID) {
        TestID = testID;
    }

    public String getMRNumber() {
        return MRNumber;
    }

    public void setMRNumber(String MRNumber) {
        this.MRNumber = MRNumber;
    }

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }


    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }

}
