package com.asania.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aqsa.sarwar on 3/18/2019.
 */

public class DevicesResult {
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Tablet")
    @Expose
    private String tablet;
    @SerializedName("IPhone")
    @Expose
    private String iPhone;
    @SerializedName("IPad")
    @Expose
    private String iPad;
    @SerializedName("Web")
    @Expose
    private String web;
    @SerializedName("AllDevices")
    @Expose
    private String aLLDEVICES;

    @SerializedName("NURSING")
    @Expose
    private String NURSING;
    @SerializedName("CONSULTANT")
    @Expose
    private String CONSULTANT;
    @SerializedName("RESIDENT")
    @Expose
    private String RESIDENT;
    @SerializedName("MEDICALOFFICER")
    @Expose
    private String MEDICALOFFICER;

    @SerializedName("HOSPITALIST")
    @Expose
    private String HOSPITALIST;
    @SerializedName("OTHERS")
    @Expose
    private String OTHERS;
    @SerializedName("TOTAL")
    @Expose
    private String TOTAL;


    private final static long serialVersionUID = 2332233766614341967L;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTablet() {
        return tablet;
    }

    public void setTablet(String tablet) {
        this.tablet = tablet;
    }

    public String getIPhone() {
        return iPhone;
    }

    public void setIPhone(String iPhone) {
        this.iPhone = iPhone;
    }

    public String getIPad() {
        return iPad;
    }

    public void setIPad(String iPad) {
        this.iPad = iPad;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getALLDEVICES() {
        return aLLDEVICES;
    }

    public String getiPhone() {
        return iPhone;
    }

    public void setiPhone(String iPhone) {
        this.iPhone = iPhone;
    }

    public String getiPad() {
        return iPad;
    }

    public void setiPad(String iPad) {
        this.iPad = iPad;
    }

    public String getaLLDEVICES() {
        return aLLDEVICES;
    }

    public void setaLLDEVICES(String aLLDEVICES) {
        this.aLLDEVICES = aLLDEVICES;
    }

    public String getNURSING() {
        return NURSING;
    }

    public void setNURSING(String NURSING) {
        this.NURSING = NURSING;
    }

    public String getCONSULTANT() {
        return CONSULTANT;
    }

    public void setCONSULTANT(String CONSULTANT) {
        this.CONSULTANT = CONSULTANT;
    }

    public String getRESIDENT() {
        return RESIDENT;
    }

    public void setRESIDENT(String RESIDENT) {
        this.RESIDENT = RESIDENT;
    }

    public String getMEDICALOFFICER() {
        return MEDICALOFFICER;
    }

    public void setMEDICALOFFICER(String MEDICALOFFICER) {
        this.MEDICALOFFICER = MEDICALOFFICER;
    }

    public String getHOSPITALIST() {
        return HOSPITALIST;
    }

    public void setHOSPITALIST(String HOSPITALIST) {
        this.HOSPITALIST = HOSPITALIST;
    }

    public String getOTHERS() {
        return OTHERS;
    }

    public void setOTHERS(String OTHERS) {
        this.OTHERS = OTHERS;
    }

    public void setALLDEVICES(String aLLDEVICES) {
        this.aLLDEVICES = aLLDEVICES;
    }

    public String getTOTAL() {
        return TOTAL;
    }

    public void setTOTAL(String TOTAL) {
        this.TOTAL = TOTAL;
    }
}
