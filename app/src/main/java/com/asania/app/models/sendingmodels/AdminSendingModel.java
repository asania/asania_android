package com.asania.app.models.sendingmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.asania.app.managers.retrofit.GsonFactory;


/**
 * Created by aqsa.sarwar on 1/2/2018.
 */

public class AdminSendingModel {


    @SerializedName("Password")
    private String password;
    @SerializedName("AccessFromDateTime")
    private String accessFromDateTime;
    @SerializedName("AccessToDateTime")
    private String accessToDateTime;
    @SerializedName("MRNumber")
    private String mrNumber;
    @SerializedName("Username")
    private String name;

    @SerializedName("UserID")
    @Expose
    private String UserId;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMrNumber() {
        return mrNumber;
    }

    public void setMrNumber(String mrNumber) {
        this.mrNumber = mrNumber;
    }

    public String getAccessFromDateTime() {
        return accessFromDateTime;
    }

    public void setAccessFromDateTime(String accessFromDateTime) {
        this.accessFromDateTime = accessFromDateTime;
    }

    public String getAccessToDateTime() {
        return accessToDateTime;
    }

    public void setAccessToDateTime(String accessToDateTime) {
        this.accessToDateTime = accessToDateTime;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }
}
