package com.asania.app.models;

import com.asania.app.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hamza.ahmed on 8/6/2018.
 */

public class AdminDisplayDevicesModel {


    private String message;
    private String title;
    @Expose
    @SerializedName("RecordFound")
    private String recordfound;
    @Expose
    @SerializedName("RegCardNo")
    private String regcardno;
    @Expose
    @SerializedName("DeviceLocation")
    private String devicelocation;
    @Expose
    @SerializedName("DeviceModel")
    private String devicemodel;
    @Expose
    @SerializedName("DeviceManufacturer")
    private String devicemanufacturer;
    @Expose
    @SerializedName("DeviceScreenSize")
    private String devicescreensize;
    @Expose
    @SerializedName("DeviceOSVersion")
    private String deviceosversion;
    @Expose
    @SerializedName("DeviceOS")
    private String deviceos;
    @Expose
    @SerializedName("DeviceType")
    private String devicetype;
    @Expose
    @SerializedName("DeviceID")
    private String deviceid;
    @Expose
    @SerializedName("LastFileDttm")
    private String lastFileDttm;

    @Expose
    @SerializedName("DeviceBlockedDTTM")
    private String DeviceBlockedDTTM;

    @Expose
    @SerializedName("DeviceBlockedBy")
    private String DeviceBlockedBy;

    @Expose
    @SerializedName("DeviceBlocked")
    private String DeviceBlocked;



    public String getLastFileDttm() {
        return lastFileDttm;
    }

    public void setLastFileDttm(String lastFileDttm) {
        this.lastFileDttm = lastFileDttm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecordfound() {
        return recordfound;
    }

    public void setRecordfound(String recordfound) {
        this.recordfound = recordfound;
    }

    public String getRegcardno() {
        return regcardno;
    }

    public void setRegcardno(String regcardno) {
        this.regcardno = regcardno;
    }

    public String getDevicelocation() {
        return devicelocation;
    }

    public void setDevicelocation(String devicelocation) {
        this.devicelocation = devicelocation;
    }

    public String getDevicemodel() {
        return devicemodel;
    }

    public void setDevicemodel(String devicemodel) {
        this.devicemodel = devicemodel;
    }

    public String getDevicemanufacturer() {
        return devicemanufacturer;
    }

    public void setDevicemanufacturer(String devicemanufacturer) {
        this.devicemanufacturer = devicemanufacturer;
    }

    public String getDevicescreensize() {
        return devicescreensize;
    }

    public void setDevicescreensize(String devicescreensize) {
        this.devicescreensize = devicescreensize;
    }

    public String getDeviceosversion() {
        return deviceosversion;
    }

    public void setDeviceosversion(String deviceosversion) {
        this.deviceosversion = deviceosversion;
    }

    public String getDeviceos() {
        return deviceos;
    }

    public void setDeviceos(String deviceos) {
        this.deviceos = deviceos;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }



    public String getDeviceBlocked() {
        return DeviceBlocked;
    }

    public void setDeviceBlocked(String deviceBlocked) {
        DeviceBlocked = deviceBlocked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDeviceBlockedDTTM() {
        return DeviceBlockedDTTM;
    }

    public void setDeviceBlockedDTTM(String deviceBlockedDTTM) {
        DeviceBlockedDTTM = deviceBlockedDTTM;
    }

    public String getDeviceBlockedBy() {
        return DeviceBlockedBy;
    }

    public void setDeviceBlockedBy(String deviceBlockedBy) {
        DeviceBlockedBy = deviceBlockedBy;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }
}