package com.asania.app.models.wrappers;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khanhamza on 09-Mar-17.
 */

public class WebResponse<T> {

//    "status": true,
//            "message": "Logged-In Successfully",
//            "paging": {},
//            "body": {


    @SerializedName("message")
    public String message;

    @SerializedName("status")
    public int status;

    @SerializedName("data")
    public T data;

    public int isSuccess() {
//        return responseCode == 200;
        return status;
    }
   /* public boolean noRecordFound() {
        return responseCode == 204;
    }
*/


}
