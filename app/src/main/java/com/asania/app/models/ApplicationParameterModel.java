package com.asania.app.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApplicationParameterModel implements Serializable {

    @SerializedName("ApplicationRoleList")
    @Expose
    private ArrayList<ApplicationRoleList> applicationRoleList = null;
    @SerializedName("ApplicationMenuList")
    @Expose
    private ArrayList<ApplicationMenuList> applicationMenuList = null;
    @SerializedName("AllApplicationMenuList")
    @Expose
    private ArrayList<AllApplicationMenuList> allApplicationMenuList = null;
    private final static long serialVersionUID = 5208677422465662136L;

    public ArrayList<ApplicationRoleList> getApplicationRoleList() {
        return applicationRoleList;
    }

    public void setApplicationRoleList(ArrayList<ApplicationRoleList> applicationRoleList) {
        this.applicationRoleList = applicationRoleList;
    }

    public ArrayList<ApplicationMenuList> getApplicationMenuList() {
        return applicationMenuList;
    }

    public void setApplicationMenuList(ArrayList<ApplicationMenuList> applicationMenuList) {
        this.applicationMenuList = applicationMenuList;
    }

    public ArrayList<AllApplicationMenuList> getAllApplicationMenuList() {
        return allApplicationMenuList;
    }

    public void setAllApplicationMenuList(ArrayList<AllApplicationMenuList> allApplicationMenuList) {
        this.allApplicationMenuList = allApplicationMenuList;
    }

}