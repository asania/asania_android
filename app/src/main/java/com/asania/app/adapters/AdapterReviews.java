package com.asania.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.asania.R;

import java.util.List;

public class AdapterReviews extends RecyclerView.Adapter<AdapterReviews.ViewHolder> {

    private LayoutInflater mInflater;
    public AdapterReviews(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterReviews.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_visitor_rating, parent, false);
        return new AdapterReviews.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterReviews.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    String getItem(int id) {
        return ""; //mData.get(id);

    }

}
