package com.asania.app.adapters;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.asania.R;
import com.asania.app.activities.BuffetsProfileActivity;

public class TourItemListAdapter extends RecyclerView.Adapter<TourItemListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    public TourItemListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public TourItemListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_home_list_item, parent, false);
        return new TourItemListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TourItemListAdapter.ViewHolder holder, int position) {
        holder.imgBanner.setImageDrawable(this.context.getResources().getDrawable(R.drawable.tour_img));
    }

    @Override
    public int getItemCount() {
        return 7;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNotification,tvTitle,tvDate;
        ImageView imgBanner;

        ViewHolder(View itemView) {
            super(itemView);
            imgBanner = (ImageView)itemView.findViewById(R.id.imgBanner);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //  openFeedBackDialog();
            context.startActivity(new Intent(context, BuffetsProfileActivity.class));
        }
    }

    String getItem(int id) {
        return ""; //mData.get(id);

    }

    public void openFeedBackDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.context,R.style.RateDialog);
        LayoutInflater myLayout = LayoutInflater.from(this.context);
        final View dialogView = myLayout.inflate(R.layout.layout_rate_venue, null);
        builder.setView(dialogView);
        Dialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }
}

