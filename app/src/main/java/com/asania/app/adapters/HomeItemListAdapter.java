package com.asania.app.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.asania.R;
import com.asania.app.activities.BuffetsProfileActivity;

public class HomeItemListAdapter extends RecyclerView.Adapter<HomeItemListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    public HomeItemListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public HomeItemListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_home_list_item, parent, false);
        return new HomeItemListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeItemListAdapter.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 7;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNotification,tvTitle,tvDate;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
          //  openFeedBackDialog();
            context.startActivity(new Intent(context, BuffetsProfileActivity.class));
        }
    }

    String getItem(int id) {
        return ""; //mData.get(id);

    }

    public void openFeedBackDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.context,R.style.RateDialog);
        LayoutInflater myLayout = LayoutInflater.from(this.context);
        final View dialogView = myLayout.inflate(R.layout.layout_rate_venue, null);
        builder.setView(dialogView);
        Dialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }
}
