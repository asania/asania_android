package com.asania.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import androidx.recyclerview.widget.RecyclerView;

import com.asania.R;
import com.github.islamkhsh.CardSliderAdapter;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterBanner extends CardSliderAdapter<AdapterBanner.BannerViewHolder> {

private List<Integer> banners;
        Context context;

public AdapterBanner(List<Integer> banners,Context context){
        this.banners = banners;
        this.context = context;
        }

@Override
public int getItemCount(){
        return 10;
        }

@Override
public BannerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_banner, parent, false);
        return new BannerViewHolder(view);
        }

@Override
public void bindVH(@NotNull BannerViewHolder bannerViewHolder, int i) {
        Picasso.with(context).load(R.drawable.hall).into(bannerViewHolder.imgBanner);
        }

class BannerViewHolder extends RecyclerView.ViewHolder {
    ImageView imgBanner;
    public BannerViewHolder(View view){
        super(view);
        imgBanner = itemView.findViewById(R.id.imgBuffets);
     }
}
}