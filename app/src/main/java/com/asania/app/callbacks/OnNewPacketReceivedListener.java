package com.asania.app.callbacks;

public interface OnNewPacketReceivedListener
{
    void onNewPacket(int event, Object data);
}
