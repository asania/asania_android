package com.asania.app.callbacks;

public interface OnItemClickListener {
    void onItemClick(int position, Object object);
}
