package com.asania.app.constatnts;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.asania.BaseApplication;
import com.asania.app.managers.SharedPreferenceManager;
import com.asania.app.models.sendingmodels.RegisteredDeviceModel;

import java.security.MessageDigest;
import java.util.UUID;

import static android.provider.Settings.Secure.getString;


/**
 * Created by khanhamza on 4/20/2017.
 */

public class AppConstants {

    public static final String INPUT_DATE_FORMAT = "yyyy-dd-MM hh:mm:ss";
    public static final String INPUT_DATE_FORMAT_AM_PM = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_DATE_FORMAT = "EEEE dd,yyyy";
    public static final String INPUT_TIME_FORMAT = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_TIME_FORMAT = "hh:mm a";
    public static final String OUTPUT_DATE_TIME_FORMAT = "EEEE dd,yyyy hh:mm a";
    public static String DEVICE_OS_ANDROID = "ANDROID";
    public static final String GENERAL_DATE_FORMAT = "dd-MM-yy";
    public static final String ROOT_MEDIA_PATH = Environment.getExternalStorageDirectory().getPath()
            + "/" + BaseApplication.getApplicationName() + "/Media";
    public static final String USER_PROFILE_PICTURE = ROOT_MEDIA_PATH + "/" + BaseApplication.getApplicationName() + " Profile";

    /*******************Preferences KEYS******************/
    public static final String USER_DATA = "userData";
    public static final String USER_NOTIFICATION_DATA = "USER_NOTIFICATION_DATA";

    public static String PROFILE_REGISTRATION = "profile_registration";
    public static String FORCED_RESTART = "forced_restart";
    public static final String KEY_REGISTER_VM = "register_vm";

    public static final String JSON_STRING_KEY = "JSON_STRING_KEY";

    //    API SERVICES
    public static final String KEY_ONE_TIME_TOKEN = "";


    /*******************Preferences KEYS******************/
    public static final String KEY_REGISTERED_DEVICE = "registered_device";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_CODE = "code";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_CURRENT_USER_MODEL = "KEY_CURRENT_USER_MODEL";

    public static RegisteredDeviceModel getRegisteredDevice(Context context, Activity activity) {
        SharedPreferenceManager sharedPreferenceManager = SharedPreferenceManager.getInstance(context);
        RegisteredDeviceModel registeredDeviceModel = sharedPreferenceManager.getObject(KEY_REGISTERED_DEVICE, RegisteredDeviceModel.class);
        if (registeredDeviceModel == null) {
            registeredDeviceModel = new RegisteredDeviceModel();
        }


        // Set Device ID
        if (registeredDeviceModel.getDeviceid() == null || registeredDeviceModel.getDeviceid().isEmpty()) {
            registeredDeviceModel.setDeviceid(getDeviceID(context));
        }

        // Set Registered Card Number Everytime
        if (sharedPreferenceManager.getString(KEY_USER_ID) != null) {
            registeredDeviceModel.setUserID(sharedPreferenceManager.getString(KEY_USER_ID));
        }


        // Getting Display Metrics only if Display values not set

        if (registeredDeviceModel.getDevicescreensize() == null || registeredDeviceModel.getDevicescreensize().isEmpty()) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            float yInches = metrics.heightPixels / metrics.ydpi;
            float xInches = metrics.widthPixels / metrics.xdpi;
            double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
            if (diagonalInches >= 6.9) {
                registeredDeviceModel.setDevicetype("Tablet");
            } else {
                registeredDeviceModel.setDevicetype("Phone");
            }
            registeredDeviceModel.setDeviceos(DEVICE_OS_ANDROID);
            registeredDeviceModel.setDevicescreensize(metrics.heightPixels + "x" + metrics.widthPixels);
            registeredDeviceModel.setDevicemanufacturer(Build.MANUFACTURER);
            registeredDeviceModel.setDevicemodel(Build.MODEL);

        }

        registeredDeviceModel.setDeviceosversion(Build.VERSION.RELEASE);

        try {
            registeredDeviceModel.setAppVersion(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("App Constants:", "Get App Version: " + e.getLocalizedMessage());
        }


        registeredDeviceModel.setUserID(sharedPreferenceManager.getString(KEY_USER_ID));
        registeredDeviceModel.setLoginCode(sharedPreferenceManager.getString(KEY_CODE));


        SharedPreferenceManager.getInstance(context).putObject(KEY_REGISTERED_DEVICE, registeredDeviceModel);
        return registeredDeviceModel;
    }

    private static String getDeviceID(Context context) {

/*String Return_DeviceID = USERNAME_and_PASSWORD.getString(DeviceID_key,"Guest");
return Return_DeviceID;*/

        TelephonyManager TelephonyMgr = (TelephonyManager) context.getApplicationContext().getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);

        String m_szImei = ""; // Requires
        if (TelephonyMgr != null) {
            m_szImei = TelephonyMgr.getDeviceId();
        }
// READ_PHONE_STATE

// 2 compute DEVICE ID
        String m_szDevIDShort = "35"
                + // we make this look like a valid IMEI
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10
                + Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10
                + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10
                + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10
                + Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10
                + Build.TAGS.length() % 10 + Build.TYPE.length() % 10
                + Build.USER.length() % 10; // 13 digits
// 3 android ID - unreliable
        String m_szAndroidID = "";
        if (getString(context.getContentResolver(), Settings.Secure.ANDROID_ID) != null) {
            m_szAndroidID = getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        String m_szLongID = m_szImei + m_szDevIDShort + m_szAndroidID;
        System.out.println("m_szLongID " + m_szLongID);
        MessageDigest m = null;

        // FIXME: 5/28/2018 commenting algo, 30 character value


        // If SHA-256
        if (m == null) {
            if (!m_szLongID.isEmpty()) {
                if (m_szLongID.length() > 30) {
                    return m_szLongID.substring(0, 30);
                } else {
                    return m_szLongID;
                }
            } else {
                return getDeviceID2(context);
            }
        }


        m.update(m_szLongID.getBytes(), 0, m_szLongID.length());
        byte p_md5Data[] = m.digest();

        String m_szUniqueID = "";
        for (int i = 0; i < p_md5Data.length; i++) {
            int b = (0xFF & p_md5Data[i]);
// if it is a single digit, make sure it have 0 in front (proper
// padding)
            if (b <= 0xF)
                m_szUniqueID += "0";
// add number to string
            m_szUniqueID += Integer.toHexString(b);
        }
        m_szUniqueID = m_szUniqueID.toUpperCase();

        Log.i("------DeviceID------", m_szUniqueID);
        Log.d("DeviceIdCheck", "DeviceId that generated MPreferenceActivity:" + m_szUniqueID);

        return m_szUniqueID;
    }

    public static String getDeviceID2(Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String tmDevice = "", tmSerial = "", androidId = "";
        UUID deviceUuid;

        if (tm != null) {
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
            androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        } else {
            androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        }

        return deviceUuid.toString();

    }



    // Custom For AKUH
    public static final String ACCESS_LOG_DATE_SHOW = "MMM dd, yyyy";
    public static final String ACCESS_LOG_DATE_SEND = "dd/MM/yyyy HH:mm:ss";
    public static final String ACCESS_LOG_DATE_SEND_START_DATE = "dd/MM/yyyy 00:00:00";
    public static final String INPUT_DATE_FORMAT_IMMUNIZATION = "dd/MM/yyyy";
}
