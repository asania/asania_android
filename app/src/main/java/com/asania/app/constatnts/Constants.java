/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.asania.app.constatnts;

import android.content.Context;
import android.content.res.Resources;

import com.asania.app.models.MenuList;

import java.util.ArrayList;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public final class Constants {


    public static ArrayList<MenuList> arrStaticMenu(Context context) {
        ArrayList<MenuList> arrayList = new ArrayList<>();

        Resources resource = context.getApplicationContext().getResources();

//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuMyPatients", "MyPatients@aku", R.drawable.mypatlogo, resource.getColor(R.color.material_red100)));
//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuFamilyHifazat", "Family Hifazat", R.drawable.app_icon_fh_blue, resource.getColor(R.color.material_blue100)));
//
//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuMyPatientsStatistics", "Admin Statistics", R.drawable.admin_stats_icon, R.drawable.rounded_box_blue));
//
//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuMyPatientsMaintenance", "Maintenance", R.drawable.mypatlogo,R.drawable.rounded_box_blue));
//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuMyPatientsTraceByUser", "Trace by User", R.drawable.user_icon, R.drawable.rounded_box_amber));
//
//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuFamilyHifazatMaintenance", "Family Hifazat Maintenance", R.drawable.app_icon_fh_blue, resource.getColor(R.color.c_white)));
//        arrayList.add(new MenuList("SDS-SUPPORT", "mnuFamilyHifazatStatistics", "Family Hifazat Statistics", R.drawable.app_icon_fh_blue, resource.getColor(R.color.c_white)));

        return arrayList;
    }
}
