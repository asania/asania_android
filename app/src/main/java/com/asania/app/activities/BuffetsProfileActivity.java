package com.asania.app.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.asania.R;
import com.asania.app.adapters.AdapterBanner;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.widget.AnyTextView;
import com.github.islamkhsh.CardSliderViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuffetsProfileActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.viewPagerBanner)
    CardSliderViewPager viewPagerBanner;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.imgVisitorRating)
    ImageView imgVisitorRating;

    @BindView(R.id.btnCheckAvailability)
    AnyTextView btnCheckAvailability;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_buffets_profile);
        ButterKnife.bind(this);
        viewPagerBanner.setAdapter(new AdapterBanner(null, BuffetsProfileActivity.this));
        btnCheckAvailability.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgVisitorRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BuffetsProfileActivity.this, ActivityVisitorsRating.class));
            }
        });
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCheckAvailability:
                startActivity(new Intent(BuffetsProfileActivity.this,ActivityCalendar.class));
                break;
        }
    }
}