package com.asania.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.asania.R;
import com.asania.app.adapters.AdapterReviews;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityVisitorsRating extends AppCompatActivity {

    @BindView(R.id.rvRatings)
    RecyclerView rvRatings;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitors_rating);
        ButterKnife.bind(this);
        rvRatings.setLayoutManager(new LinearLayoutManager(ActivityVisitorsRating.this));
        AdapterReviews adapterNotification = new AdapterReviews(ActivityVisitorsRating.this);
        rvRatings.setAdapter(adapterNotification);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}