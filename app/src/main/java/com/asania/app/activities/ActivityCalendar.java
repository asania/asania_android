package com.asania.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.CalendarView;

import com.asania.R;
import com.asania.app.widget.AnyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCalendar extends AppCompatActivity {
    @BindView(R.id.simpleDatePicker)
    CalendarView simpleDatePicker;

    @BindView(R.id.btnContinue)
    AnyTextView btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFeedBackDialog();
              //  startActivity(new Intent(ActivityCalendar.this,ActivityBookingForm.class));
            }
        });
        //simpleDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    public void openFeedBackDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCalendar.this,R.style.RateDialog);
        LayoutInflater myLayout = LayoutInflater.from(ActivityCalendar.this);
        final View dialogView = myLayout.inflate(R.layout.activity_booking_form, null);
        builder.setView(dialogView);
        Dialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }
}