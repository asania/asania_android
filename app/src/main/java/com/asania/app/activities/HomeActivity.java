package com.asania.app.activities;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.asania.R;


import com.asania.app.constatnts.AppConstants;
import com.asania.app.fragments.DashboardFragment;
import com.asania.app.helperclasses.ui.helper.TitleBar;
import com.asania.app.helperclasses.ui.helper.UIHelper;
import com.asania.app.managers.SharedPreferenceManager;
import com.asania.app.models.User.UserModel;


public class HomeActivity extends BaseActivity {


    RelativeLayout contParentActivityLayout;
    private UserModel userModel;
    private SharedPreferenceManager sharedPreferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contParentActivityLayout = findViewById(R.id.contParentActivityLayout);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);
        userModel = sharedPreferenceManager.getCurrentUser();
        titleBar = (TitleBar)findViewById(R.id.titlebar);
        titleBar.getBtnLeft2().setVisibility(View.VISIBLE);
        titleBar.getBtnLeft2().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.openDrawer(GravityCompat.START);
                else drawerLayout.closeDrawer(GravityCompat.END);
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        UserModel userModel = SharedPreferenceManager.getInstance(this).getObject(AppConstants.KEY_CURRENT_USER_MODEL, UserModel.class);
        initFragments(/*UserTypeEnum.fromCanonicalForm(userModel.getCssausermodel().getRole())*/);
    }

    public RelativeLayout getContParentActivityLayout() {
        return contParentActivityLayout;
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_home;
    }

    @Override
    protected int getTitlebarLayoutId() {
        return R.id.titlebar;
    }



    @Override
    protected int getDrawerLayoutId() {
        return R.id.drawer_layout;
    }

    @Override
    protected int getDrawerFragmentId() {
        return R.id.contDrawer;
    }

    @Override
    protected int getDockableFragmentId() {
        return R.id.contMain;
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    // FIXME: 26/07/2018 Use Enum UserType instead of String
    private void initFragments() {
            addDockableFragment(DashboardFragment.newInstance(), false);

    }


    @Override
    public void onBackPressed() {
        if (isInSelectingState) {
            if (genericClickableInterface != null) {
                genericClickableInterface.click();
                isInSelectingState = false;
            } else {
                UIHelper.showToast(getBaseContext(), "No call back selected.");
            }
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            closeApp();
        }
    }

}