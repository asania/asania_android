package com.asania.app.activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import com.asania.R;
import com.asania.app.helperclasses.ui.helper.AnimationHelper;
import com.asania.app.managers.SharedPreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;


import static android.view.View.VISIBLE;

public class SplashActivity extends Activity {
    private final int ANIMATIONS_DELAY = 250;

    private final int SPLASH_TIME_OUT = 500;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    SharedPreferenceManager sharedPreferenceManager;
    boolean isUserLoggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);
        isUserLoggedIn = sharedPreferenceManager.getCurrentUser() != null;

        // fixing portrait mode problem for SDK 26 if using windowIsTranslucent = true
        if (Build.VERSION.SDK_INT == 26) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        contParentLayout.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                animateLayout(SPLASH_TIME_OUT);
            }
        }, ANIMATIONS_DELAY);


    }

    private void animateLayout(int SPLASH_TIME_OUT) {
        AnimationHelper.fade(contParentLayout, 0, VISIBLE, VISIBLE, 1, SPLASH_TIME_OUT, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                    @Override
                    public void run() {

                        // This method will be executed once the timer is over
                        // Start your app main activity
//                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
//                        startActivity(i);
                        isUserLoggedIn = sharedPreferenceManager.getCurrentUser() != null;
                        Intent i;
                        if (isUserLoggedIn) {
                            i = new Intent(SplashActivity.this, HomeActivity.class);
                        } else {
                            i = new Intent(SplashActivity.this, MainActivity.class);
                        }

                        startActivity(i);
                        // close this activity
                        finish();
                    }
                }, 1000);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }




}
