package com.asania.app.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asania.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySettings extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvLanguage)
    TextView tvLanguage;

    @BindView(R.id.tvTerms)
    TextView tvTerms;

    @BindView(R.id.tvPolicy)
    TextView tvPolicy;

    @BindView(R.id.layoutLanguage)
    RelativeLayout layoutLanguage;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        toolbar.setTitle("Settings");
        layoutLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLanguage();
            }
        });

    }


    public void changeLanguage() {
        dialogWithOptions(tvLanguage);
    }
    public void dialogWithOptions(final TextView tvLanguage){
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySettings.this);
        builder.setTitle("Language");
        final String[] options = {"English", "Chinese","Arabic","Urdu"};
        int checkedItem = 0;
        builder.setSingleChoiceItems(options, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvLanguage.setText(options[which]);
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}