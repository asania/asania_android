// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.asania.R;
import com.google.android.material.tabs.TabLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DashboardFragment_ViewBinding implements Unbinder {
  private DashboardFragment target;

  @UiThread
  public DashboardFragment_ViewBinding(DashboardFragment target, View source) {
    this.target = target;

    target.contParentLayout = Utils.findRequiredViewAsType(source, R.id.contParentLayout, "field 'contParentLayout'", LinearLayout.class);
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.viewPager, "field 'viewPager'", ViewPager.class);
    target.tabLayout = Utils.findRequiredViewAsType(source, R.id.tabLayout, "field 'tabLayout'", TabLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DashboardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.contParentLayout = null;
    target.viewPager = null;
    target.tabLayout = null;
  }
}
