// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments.dialogs;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PermissionDialogFragment_ViewBinding implements Unbinder {
  private PermissionDialogFragment target;

  private View view7f09006f;

  @UiThread
  public PermissionDialogFragment_ViewBinding(final PermissionDialogFragment target, View source) {
    this.target = target;

    View view;
    target.txtPermission = Utils.findRequiredViewAsType(source, R.id.txtPermission, "field 'txtPermission'", AnyTextView.class);
    view = Utils.findRequiredView(source, R.id.btnOK, "field 'btnOK' and method 'onViewClicked'");
    target.btnOK = Utils.castView(view, R.id.btnOK, "field 'btnOK'", AnyTextView.class);
    view7f09006f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PermissionDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtPermission = null;
    target.btnOK = null;

    view7f09006f.setOnClickListener(null);
    view7f09006f = null;
  }
}
