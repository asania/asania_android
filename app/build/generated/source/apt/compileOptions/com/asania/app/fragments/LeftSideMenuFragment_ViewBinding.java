// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LeftSideMenuFragment_ViewBinding implements Unbinder {
  private LeftSideMenuFragment target;

  private View view7f0901f5;

  private View view7f0901f1;

  private View view7f0901ef;

  private View view7f0901fb;

  private View view7f0901f6;

  private View view7f0901fa;

  @UiThread
  public LeftSideMenuFragment_ViewBinding(final LeftSideMenuFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.txtHome, "field 'txtHome' and method 'onViewClicked'");
    target.txtHome = Utils.castView(view, R.id.txtHome, "field 'txtHome'", AnyTextView.class);
    view7f0901f5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtCardSubscription, "field 'txtCardSubscription' and method 'onViewClicked'");
    target.txtCardSubscription = Utils.castView(view, R.id.txtCardSubscription, "field 'txtCardSubscription'", AnyTextView.class);
    view7f0901f1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtAbout, "field 'txtAbout' and method 'onViewClicked'");
    target.txtAbout = Utils.castView(view, R.id.txtAbout, "field 'txtAbout'", AnyTextView.class);
    view7f0901ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtSettings, "field 'txtSettings' and method 'onViewClicked'");
    target.txtSettings = Utils.castView(view, R.id.txtSettings, "field 'txtSettings'", AnyTextView.class);
    view7f0901fb = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtLogout, "field 'txtLogout' and method 'onViewClicked'");
    target.txtLogout = Utils.castView(view, R.id.txtLogout, "field 'txtLogout'", AnyTextView.class);
    view7f0901f6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", ScrollView.class);
    target.imgBackground = Utils.findRequiredViewAsType(source, R.id.imgBackground, "field 'imgBackground'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.txtProfile, "method 'onViewClicked'");
    view7f0901fa = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LeftSideMenuFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtHome = null;
    target.txtCardSubscription = null;
    target.txtAbout = null;
    target.txtSettings = null;
    target.txtLogout = null;
    target.scrollView = null;
    target.imgBackground = null;

    view7f0901f5.setOnClickListener(null);
    view7f0901f5 = null;
    view7f0901f1.setOnClickListener(null);
    view7f0901f1 = null;
    view7f0901ef.setOnClickListener(null);
    view7f0901ef = null;
    view7f0901fb.setOnClickListener(null);
    view7f0901fb = null;
    view7f0901f6.setOnClickListener(null);
    view7f0901f6 = null;
    view7f0901fa.setOnClickListener(null);
    view7f0901fa = null;
  }
}
