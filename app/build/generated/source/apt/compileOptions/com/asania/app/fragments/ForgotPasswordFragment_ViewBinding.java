// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.asania.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ForgotPasswordFragment_ViewBinding implements Unbinder {
  private ForgotPasswordFragment target;

  private View view7f090073;

  private View view7f090074;

  private View view7f090072;

  @UiThread
  public ForgotPasswordFragment_ViewBinding(final ForgotPasswordFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnSignIn, "method 'onViewClicked'");
    view7f090073 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnSignUp, "method 'onViewClicked'");
    view7f090074 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnSend, "method 'onViewClicked'");
    view7f090072 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view7f090073.setOnClickListener(null);
    view7f090073 = null;
    view7f090074.setOnClickListener(null);
    view7f090074 = null;
    view7f090072.setOnClickListener(null);
    view7f090072 = null;
  }
}
