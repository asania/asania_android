// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyEditTextView;
import com.asania.app.widget.AnyTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginFragment_ViewBinding implements Unbinder {
  private LoginFragment target;

  private View view7f090074;

  private View view7f0901f7;

  private View view7f09006e;

  private View view7f090075;

  private View view7f09006b;

  @UiThread
  public LoginFragment_ViewBinding(final LoginFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnSignUp, "field 'btnSignUp' and method 'onViewClicked'");
    target.btnSignUp = Utils.castView(view, R.id.btnSignUp, "field 'btnSignUp'", AnyTextView.class);
    view7f090074 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.edtEmailAddress = Utils.findRequiredViewAsType(source, R.id.edtEmailAddress, "field 'edtEmailAddress'", AnyEditTextView.class);
    target.edtPassword = Utils.findRequiredViewAsType(source, R.id.edtPassword, "field 'edtPassword'", AnyEditTextView.class);
    view = Utils.findRequiredView(source, R.id.txtForgotPassword, "field 'txtForgotPassword' and method 'onViewClicked'");
    target.txtForgotPassword = Utils.castView(view, R.id.txtForgotPassword, "field 'txtForgotPassword'", AnyTextView.class);
    view7f0901f7 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkShowPassword = Utils.findRequiredViewAsType(source, R.id.chkShowPassword, "field 'chkShowPassword'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.btnLogin, "field 'btnLogin' and method 'onViewClicked'");
    target.btnLogin = Utils.castView(view, R.id.btnLogin, "field 'btnLogin'", AnyTextView.class);
    view7f09006e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.contContent = Utils.findRequiredViewAsType(source, R.id.contContent, "field 'contContent'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btnfb, "field 'btnfb' and method 'onViewClicked'");
    target.btnfb = Utils.castView(view, R.id.btnfb, "field 'btnfb'", ImageView.class);
    view7f090075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnGmail, "field 'btnGmail' and method 'onViewClicked'");
    target.btnGmail = Utils.castView(view, R.id.btnGmail, "field 'btnGmail'", ImageView.class);
    view7f09006b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.contLogin = Utils.findRequiredViewAsType(source, R.id.contLogin, "field 'contLogin'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnSignUp = null;
    target.edtEmailAddress = null;
    target.edtPassword = null;
    target.txtForgotPassword = null;
    target.chkShowPassword = null;
    target.btnLogin = null;
    target.contContent = null;
    target.btnfb = null;
    target.btnGmail = null;
    target.contLogin = null;

    view7f090074.setOnClickListener(null);
    view7f090074 = null;
    view7f0901f7.setOnClickListener(null);
    view7f0901f7 = null;
    view7f09006e.setOnClickListener(null);
    view7f09006e = null;
    view7f090075.setOnClickListener(null);
    view7f090075 = null;
    view7f09006b.setOnClickListener(null);
    view7f09006b = null;
  }
}
