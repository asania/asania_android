// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyEditTextView;
import com.asania.app.widget.AnyTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignUpFragment_ViewBinding implements Unbinder {
  private SignUpFragment target;

  private View view7f090073;

  private View view7f090074;

  @UiThread
  public SignUpFragment_ViewBinding(final SignUpFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnSignIn, "field 'btnSignIn' and method 'onViewClicked'");
    target.btnSignIn = Utils.castView(view, R.id.btnSignIn, "field 'btnSignIn'", AnyTextView.class);
    view7f090073 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.edtFullName = Utils.findRequiredViewAsType(source, R.id.edtFullName, "field 'edtFullName'", AnyEditTextView.class);
    target.edtEmailAddress = Utils.findRequiredViewAsType(source, R.id.edtEmailAddress, "field 'edtEmailAddress'", AnyEditTextView.class);
    target.txtCity = Utils.findRequiredViewAsType(source, R.id.txtCity, "field 'txtCity'", AnyTextView.class);
    target.edtPhoneNumber = Utils.findRequiredViewAsType(source, R.id.edtPhoneNumber, "field 'edtPhoneNumber'", AnyEditTextView.class);
    target.edtPassword = Utils.findRequiredViewAsType(source, R.id.edtPassword, "field 'edtPassword'", AnyEditTextView.class);
    target.chkShowPassword = Utils.findRequiredViewAsType(source, R.id.chkShowPassword, "field 'chkShowPassword'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.btnSignUp, "field 'btnSignUp' and method 'onViewClicked'");
    target.btnSignUp = Utils.castView(view, R.id.btnSignUp, "field 'btnSignUp'", AnyTextView.class);
    view7f090074 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.contLogin = Utils.findRequiredViewAsType(source, R.id.contLogin, "field 'contLogin'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SignUpFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnSignIn = null;
    target.edtFullName = null;
    target.edtEmailAddress = null;
    target.txtCity = null;
    target.edtPhoneNumber = null;
    target.edtPassword = null;
    target.chkShowPassword = null;
    target.btnSignUp = null;
    target.contLogin = null;

    view7f090073.setOnClickListener(null);
    view7f090073 = null;
    view7f090074.setOnClickListener(null);
    view7f090074 = null;
  }
}
