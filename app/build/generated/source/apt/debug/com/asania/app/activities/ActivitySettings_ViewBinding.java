// Generated code from Butter Knife. Do not modify!
package com.asania.app.activities;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.asania.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivitySettings_ViewBinding implements Unbinder {
  private ActivitySettings target;

  @UiThread
  public ActivitySettings_ViewBinding(ActivitySettings target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivitySettings_ViewBinding(ActivitySettings target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.tvLanguage = Utils.findRequiredViewAsType(source, R.id.tvLanguage, "field 'tvLanguage'", TextView.class);
    target.tvTerms = Utils.findRequiredViewAsType(source, R.id.tvTerms, "field 'tvTerms'", TextView.class);
    target.tvPolicy = Utils.findRequiredViewAsType(source, R.id.tvPolicy, "field 'tvPolicy'", TextView.class);
    target.layoutLanguage = Utils.findRequiredViewAsType(source, R.id.layoutLanguage, "field 'layoutLanguage'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivitySettings target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.tvLanguage = null;
    target.tvTerms = null;
    target.tvPolicy = null;
    target.layoutLanguage = null;
  }
}
