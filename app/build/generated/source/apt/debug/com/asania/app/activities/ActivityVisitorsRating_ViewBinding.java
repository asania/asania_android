// Generated code from Butter Knife. Do not modify!
package com.asania.app.activities;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.asania.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivityVisitorsRating_ViewBinding implements Unbinder {
  private ActivityVisitorsRating target;

  @UiThread
  public ActivityVisitorsRating_ViewBinding(ActivityVisitorsRating target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivityVisitorsRating_ViewBinding(ActivityVisitorsRating target, View source) {
    this.target = target;

    target.rvRatings = Utils.findRequiredViewAsType(source, R.id.rvRatings, "field 'rvRatings'", RecyclerView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivityVisitorsRating target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvRatings = null;
    target.toolbar = null;
  }
}
