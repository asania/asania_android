// Generated code from Butter Knife. Do not modify!
package com.asania.app.activities;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.asania.R;
import com.google.android.material.textfield.TextInputEditText;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivityProfile_ViewBinding implements Unbinder {
  private ActivityProfile target;

  @UiThread
  public ActivityProfile_ViewBinding(ActivityProfile target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivityProfile_ViewBinding(ActivityProfile target, View source) {
    this.target = target;

    target.tvChangePassword = Utils.findRequiredViewAsType(source, R.id.tvChangePassword, "field 'tvChangePassword'", TextInputEditText.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivityProfile target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvChangePassword = null;
    target.toolbar = null;
  }
}
