// Generated code from Butter Knife. Do not modify!
package com.asania.app.fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LeftSideMenuFragment_ViewBinding implements Unbinder {
  private LeftSideMenuFragment target;

  private View view7f0901f8;

  private View view7f0901f4;

  private View view7f0901f2;

  private View view7f0901fe;

  private View view7f0901f9;

  private View view7f0901fd;

  @UiThread
  public LeftSideMenuFragment_ViewBinding(final LeftSideMenuFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.txtHome, "field 'txtHome' and method 'onViewClicked'");
    target.txtHome = Utils.castView(view, R.id.txtHome, "field 'txtHome'", AnyTextView.class);
    view7f0901f8 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtCardSubscription, "field 'txtCardSubscription' and method 'onViewClicked'");
    target.txtCardSubscription = Utils.castView(view, R.id.txtCardSubscription, "field 'txtCardSubscription'", AnyTextView.class);
    view7f0901f4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtAbout, "field 'txtAbout' and method 'onViewClicked'");
    target.txtAbout = Utils.castView(view, R.id.txtAbout, "field 'txtAbout'", AnyTextView.class);
    view7f0901f2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtSettings, "field 'txtSettings' and method 'onViewClicked'");
    target.txtSettings = Utils.castView(view, R.id.txtSettings, "field 'txtSettings'", AnyTextView.class);
    view7f0901fe = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txtLogout, "field 'txtLogout' and method 'onViewClicked'");
    target.txtLogout = Utils.castView(view, R.id.txtLogout, "field 'txtLogout'", AnyTextView.class);
    view7f0901f9 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", ScrollView.class);
    target.imgBackground = Utils.findRequiredViewAsType(source, R.id.imgBackground, "field 'imgBackground'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.txtProfile, "method 'onViewClicked'");
    view7f0901fd = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LeftSideMenuFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtHome = null;
    target.txtCardSubscription = null;
    target.txtAbout = null;
    target.txtSettings = null;
    target.txtLogout = null;
    target.scrollView = null;
    target.imgBackground = null;

    view7f0901f8.setOnClickListener(null);
    view7f0901f8 = null;
    view7f0901f4.setOnClickListener(null);
    view7f0901f4 = null;
    view7f0901f2.setOnClickListener(null);
    view7f0901f2 = null;
    view7f0901fe.setOnClickListener(null);
    view7f0901fe = null;
    view7f0901f9.setOnClickListener(null);
    view7f0901f9 = null;
    view7f0901fd.setOnClickListener(null);
    view7f0901fd = null;
  }
}
