// Generated code from Butter Knife. Do not modify!
package com.asania.app.activities;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyTextView;
import com.github.islamkhsh.CardSliderViewPager;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BuffetsProfileActivity_ViewBinding implements Unbinder {
  private BuffetsProfileActivity target;

  @UiThread
  public BuffetsProfileActivity_ViewBinding(BuffetsProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BuffetsProfileActivity_ViewBinding(BuffetsProfileActivity target, View source) {
    this.target = target;

    target.viewPagerBanner = Utils.findRequiredViewAsType(source, R.id.viewPagerBanner, "field 'viewPagerBanner'", CardSliderViewPager.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.imgVisitorRating = Utils.findRequiredViewAsType(source, R.id.imgVisitorRating, "field 'imgVisitorRating'", ImageView.class);
    target.btnCheckAvailability = Utils.findRequiredViewAsType(source, R.id.btnCheckAvailability, "field 'btnCheckAvailability'", AnyTextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BuffetsProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewPagerBanner = null;
    target.toolbar = null;
    target.imgVisitorRating = null;
    target.btnCheckAvailability = null;
  }
}
