// Generated code from Butter Knife. Do not modify!
package com.asania.app.activities;

import android.view.View;
import android.widget.CalendarView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.asania.R;
import com.asania.app.widget.AnyTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivityCalendar_ViewBinding implements Unbinder {
  private ActivityCalendar target;

  @UiThread
  public ActivityCalendar_ViewBinding(ActivityCalendar target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivityCalendar_ViewBinding(ActivityCalendar target, View source) {
    this.target = target;

    target.simpleDatePicker = Utils.findRequiredViewAsType(source, R.id.simpleDatePicker, "field 'simpleDatePicker'", CalendarView.class);
    target.btnContinue = Utils.findRequiredViewAsType(source, R.id.btnContinue, "field 'btnContinue'", AnyTextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivityCalendar target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.simpleDatePicker = null;
    target.btnContinue = null;
  }
}
