/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.warkiz.widget;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int isb_clear_default_padding = 0x7f0401df;
        public static final int isb_forbid_user_seek = 0x7f0401e0;
        public static final int isb_indicator_color = 0x7f0401e1;
        public static final int isb_indicator_custom_layout = 0x7f0401e2;
        public static final int isb_indicator_custom_top_content_layout = 0x7f0401e3;
        public static final int isb_indicator_stay = 0x7f0401e4;
        public static final int isb_indicator_text_color = 0x7f0401e5;
        public static final int isb_indicator_text_size = 0x7f0401e6;
        public static final int isb_indicator_type = 0x7f0401e7;
        public static final int isb_max = 0x7f0401e8;
        public static final int isb_min = 0x7f0401e9;
        public static final int isb_progress = 0x7f0401ea;
        public static final int isb_progress_value_float = 0x7f0401eb;
        public static final int isb_seek_bar_type = 0x7f0401ec;
        public static final int isb_show_indicator = 0x7f0401ed;
        public static final int isb_text_array = 0x7f0401ee;
        public static final int isb_text_color = 0x7f0401ef;
        public static final int isb_text_left_end = 0x7f0401f0;
        public static final int isb_text_right_end = 0x7f0401f1;
        public static final int isb_text_size = 0x7f0401f2;
        public static final int isb_text_typeface = 0x7f0401f3;
        public static final int isb_thumb_color = 0x7f0401f4;
        public static final int isb_thumb_drawable = 0x7f0401f5;
        public static final int isb_thumb_progress_stay = 0x7f0401f6;
        public static final int isb_thumb_width = 0x7f0401f7;
        public static final int isb_tick_both_end_hide = 0x7f0401f8;
        public static final int isb_tick_color = 0x7f0401f9;
        public static final int isb_tick_drawable = 0x7f0401fa;
        public static final int isb_tick_num = 0x7f0401fb;
        public static final int isb_tick_on_thumb_left_hide = 0x7f0401fc;
        public static final int isb_tick_size = 0x7f0401fd;
        public static final int isb_tick_type = 0x7f0401fe;
        public static final int isb_touch_to_seek = 0x7f0401ff;
        public static final int isb_track_background_bar_color = 0x7f040200;
        public static final int isb_track_background_bar_size = 0x7f040201;
        public static final int isb_track_progress_bar_color = 0x7f040202;
        public static final int isb_track_progress_bar_size = 0x7f040203;
        public static final int isb_track_rounded_corners = 0x7f040204;
    }
    public static final class drawable {
        private drawable() {}

        public static final int isb_indicator_rounded_corners = 0x7f0800c0;
        public static final int isb_indicator_square_corners = 0x7f0800c1;
    }
    public static final class id {
        private id() {}

        public static final int circular_bubble = 0x7f090086;
        public static final int continuous = 0x7f090099;
        public static final int continuous_texts_ends = 0x7f09009a;
        public static final int custom = 0x7f0900a5;
        public static final int discrete_ticks = 0x7f0900c1;
        public static final int discrete_ticks_texts = 0x7f0900c2;
        public static final int discrete_ticks_texts_ends = 0x7f0900c3;
        public static final int indicator_arrow = 0x7f090118;
        public static final int indicator_container = 0x7f090119;
        public static final int isb_progress = 0x7f09011e;
        public static final int monospace = 0x7f090139;
        public static final int none = 0x7f090143;
        public static final int normal = 0x7f090144;
        public static final int oval = 0x7f090151;
        public static final int rec = 0x7f090175;
        public static final int rectangle = 0x7f090176;
        public static final int rectangle_rounded_corner = 0x7f090177;
        public static final int sans = 0x7f090183;
        public static final int serif = 0x7f09019d;
    }
    public static final class layout {
        private layout() {}

        public static final int isb_indicator = 0x7f0c0048;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] IndicatorSeekBar = { 0x7f0401df, 0x7f0401e0, 0x7f0401e1, 0x7f0401e2, 0x7f0401e3, 0x7f0401e4, 0x7f0401e5, 0x7f0401e6, 0x7f0401e7, 0x7f0401e8, 0x7f0401e9, 0x7f0401ea, 0x7f0401eb, 0x7f0401ec, 0x7f0401ed, 0x7f0401ee, 0x7f0401ef, 0x7f0401f0, 0x7f0401f1, 0x7f0401f2, 0x7f0401f3, 0x7f0401f4, 0x7f0401f5, 0x7f0401f6, 0x7f0401f7, 0x7f0401f8, 0x7f0401f9, 0x7f0401fa, 0x7f0401fb, 0x7f0401fc, 0x7f0401fd, 0x7f0401fe, 0x7f0401ff, 0x7f040200, 0x7f040201, 0x7f040202, 0x7f040203, 0x7f040204 };
        public static final int IndicatorSeekBar_isb_clear_default_padding = 0;
        public static final int IndicatorSeekBar_isb_forbid_user_seek = 1;
        public static final int IndicatorSeekBar_isb_indicator_color = 2;
        public static final int IndicatorSeekBar_isb_indicator_custom_layout = 3;
        public static final int IndicatorSeekBar_isb_indicator_custom_top_content_layout = 4;
        public static final int IndicatorSeekBar_isb_indicator_stay = 5;
        public static final int IndicatorSeekBar_isb_indicator_text_color = 6;
        public static final int IndicatorSeekBar_isb_indicator_text_size = 7;
        public static final int IndicatorSeekBar_isb_indicator_type = 8;
        public static final int IndicatorSeekBar_isb_max = 9;
        public static final int IndicatorSeekBar_isb_min = 10;
        public static final int IndicatorSeekBar_isb_progress = 11;
        public static final int IndicatorSeekBar_isb_progress_value_float = 12;
        public static final int IndicatorSeekBar_isb_seek_bar_type = 13;
        public static final int IndicatorSeekBar_isb_show_indicator = 14;
        public static final int IndicatorSeekBar_isb_text_array = 15;
        public static final int IndicatorSeekBar_isb_text_color = 16;
        public static final int IndicatorSeekBar_isb_text_left_end = 17;
        public static final int IndicatorSeekBar_isb_text_right_end = 18;
        public static final int IndicatorSeekBar_isb_text_size = 19;
        public static final int IndicatorSeekBar_isb_text_typeface = 20;
        public static final int IndicatorSeekBar_isb_thumb_color = 21;
        public static final int IndicatorSeekBar_isb_thumb_drawable = 22;
        public static final int IndicatorSeekBar_isb_thumb_progress_stay = 23;
        public static final int IndicatorSeekBar_isb_thumb_width = 24;
        public static final int IndicatorSeekBar_isb_tick_both_end_hide = 25;
        public static final int IndicatorSeekBar_isb_tick_color = 26;
        public static final int IndicatorSeekBar_isb_tick_drawable = 27;
        public static final int IndicatorSeekBar_isb_tick_num = 28;
        public static final int IndicatorSeekBar_isb_tick_on_thumb_left_hide = 29;
        public static final int IndicatorSeekBar_isb_tick_size = 30;
        public static final int IndicatorSeekBar_isb_tick_type = 31;
        public static final int IndicatorSeekBar_isb_touch_to_seek = 32;
        public static final int IndicatorSeekBar_isb_track_background_bar_color = 33;
        public static final int IndicatorSeekBar_isb_track_background_bar_size = 34;
        public static final int IndicatorSeekBar_isb_track_progress_bar_color = 35;
        public static final int IndicatorSeekBar_isb_track_progress_bar_size = 36;
        public static final int IndicatorSeekBar_isb_track_rounded_corners = 37;
    }
}
