/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package me.tankery.lib.circularseekbar;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int cs_circle_color = 0x7f04010f;
        public static final int cs_circle_fill = 0x7f040110;
        public static final int cs_circle_progress_color = 0x7f040111;
        public static final int cs_circle_stroke_width = 0x7f040112;
        public static final int cs_circle_style = 0x7f040113;
        public static final int cs_circle_x_radius = 0x7f040114;
        public static final int cs_circle_y_radius = 0x7f040115;
        public static final int cs_disable_pointer = 0x7f040116;
        public static final int cs_end_angle = 0x7f040117;
        public static final int cs_lock_enabled = 0x7f040118;
        public static final int cs_maintain_equal_circle = 0x7f040119;
        public static final int cs_max = 0x7f04011a;
        public static final int cs_move_outside_circle = 0x7f04011b;
        public static final int cs_negative_enabled = 0x7f04011c;
        public static final int cs_pointer_alpha_ontouch = 0x7f04011d;
        public static final int cs_pointer_angle = 0x7f04011e;
        public static final int cs_pointer_color = 0x7f04011f;
        public static final int cs_pointer_halo_border_width = 0x7f040120;
        public static final int cs_pointer_halo_color = 0x7f040121;
        public static final int cs_pointer_halo_color_ontouch = 0x7f040122;
        public static final int cs_pointer_halo_width = 0x7f040123;
        public static final int cs_pointer_stroke_width = 0x7f040124;
        public static final int cs_progress = 0x7f040125;
        public static final int cs_start_angle = 0x7f040126;
        public static final int cs_use_custom_radii = 0x7f040127;
    }
    public static final class id {
        private id() {}

        public static final int butt = 0x7f090076;
        public static final int round = 0x7f090180;
        public static final int square = 0x7f0901b0;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] cs_CircularSeekBar = { 0x7f04010f, 0x7f040110, 0x7f040111, 0x7f040112, 0x7f040113, 0x7f040114, 0x7f040115, 0x7f040116, 0x7f040117, 0x7f040118, 0x7f040119, 0x7f04011a, 0x7f04011b, 0x7f04011c, 0x7f04011d, 0x7f04011e, 0x7f04011f, 0x7f040120, 0x7f040121, 0x7f040122, 0x7f040123, 0x7f040124, 0x7f040125, 0x7f040126, 0x7f040127 };
        public static final int cs_CircularSeekBar_cs_circle_color = 0;
        public static final int cs_CircularSeekBar_cs_circle_fill = 1;
        public static final int cs_CircularSeekBar_cs_circle_progress_color = 2;
        public static final int cs_CircularSeekBar_cs_circle_stroke_width = 3;
        public static final int cs_CircularSeekBar_cs_circle_style = 4;
        public static final int cs_CircularSeekBar_cs_circle_x_radius = 5;
        public static final int cs_CircularSeekBar_cs_circle_y_radius = 6;
        public static final int cs_CircularSeekBar_cs_disable_pointer = 7;
        public static final int cs_CircularSeekBar_cs_end_angle = 8;
        public static final int cs_CircularSeekBar_cs_lock_enabled = 9;
        public static final int cs_CircularSeekBar_cs_maintain_equal_circle = 10;
        public static final int cs_CircularSeekBar_cs_max = 11;
        public static final int cs_CircularSeekBar_cs_move_outside_circle = 12;
        public static final int cs_CircularSeekBar_cs_negative_enabled = 13;
        public static final int cs_CircularSeekBar_cs_pointer_alpha_ontouch = 14;
        public static final int cs_CircularSeekBar_cs_pointer_angle = 15;
        public static final int cs_CircularSeekBar_cs_pointer_color = 16;
        public static final int cs_CircularSeekBar_cs_pointer_halo_border_width = 17;
        public static final int cs_CircularSeekBar_cs_pointer_halo_color = 18;
        public static final int cs_CircularSeekBar_cs_pointer_halo_color_ontouch = 19;
        public static final int cs_CircularSeekBar_cs_pointer_halo_width = 20;
        public static final int cs_CircularSeekBar_cs_pointer_stroke_width = 21;
        public static final int cs_CircularSeekBar_cs_progress = 22;
        public static final int cs_CircularSeekBar_cs_start_angle = 23;
        public static final int cs_CircularSeekBar_cs_use_custom_radii = 24;
    }
}
